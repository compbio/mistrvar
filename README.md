**MiStrVar**: Micro Structural Variant Caller
===================
### What is MiStrVar?
MiStrVar (Micro Structural Variant Caller) is a computational tool for detecting micro structural variations based on one-end anchors (OEA) from paired-end Whole Genome Sequencing (WGS) reads.

### How do I get MiStrVar?
Just clone our repository and issue the `make` command:
```
git clone --recursive https://git@bitbucket.org/compbio/mistrvar.git
cd mistrvar/pipeline && make
```

You can also download the latest version of MiStrVar directly from
[the Downloads link](https://bitbucket.org/compbio/mistrvar/downloads/).

> **Note**: You will need at least g++ 4.7 and Python 2.7 to compile the source code.

### How do I run MiStrVar?
You can use 
```
mistrvar/pipeline/mistrvar.py -h
```
to get a description of each parameter. For more details, please check doc/MiStrVar_manual.pdf.

#### Simulation Datasets Used for Evaluation
Please check [this link](https://goo.gl/cRDLYE) to download the simulation datasets that we used for evaluating MiStrVar. The folder contains 2 data files:
1. SV.fastq.gz: a 3.3GB gzipped fastq file 
2. chr1.fa: reference genome for chromosome 1 from GRCh37.

We also provide a checksum file ***md5.sum*** for checking file integrity.

To run MiStrVar on this dataset, type

```
./mistrvar.py -p my_project -r chr1.fa --files fastq=SV.fastq.gz
```

Feel free to change **my_project** to any name you want. Remember to specify the paths correctly for ***chr1.fa*** and ***SV.fastq.gz*** if you download these 2 files to a folder other than the one you run  MiStrVar. The VCF file with the prediction results will be generated in **my_project**.


---


### Contact & Support

Feel free to drop any inquiry to [agawrons at sfu dot ca](mailto:).
