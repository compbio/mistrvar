#!/bin/bash

cat minv-pipeline/$1/jobs/*.formatted_* > minv-pipeline/$1/$1.formatted
size=$(wc -l minv-pipeline/$1/$1.formatted | awk '{print int(($1/4)/'$2')*4}')
split -l $size minv-pipeline/$1/$1.formatted minv-pipeline/$1/jobs/$1.prep_

for i in minv-pipeline/$1/jobs/$1.prep_?? ;
do  
	qsub -cwd -V -b y -N $(basename $i) -l h_vmem=16G -l h_rt=06:00:00 -l h_stack=8M  ./minv -p=1 -m=5 -L=35 $i 
done

# sleep 5
# s=1
# while [ $s -ne '0' ] ;
# do
# 	q=0
# 	for i in minv-pipeline/$1/jobs/$1.prep_?? ;
# 	do
# 		qstat -j $(basename $i) | grep 'exist'
# 		if [ $? -eq 0 ] ;
# 		then
# 			q=$(($q+1)) ;
# 		fi
# 	done
# 	if [ $q -eq '0' ] ;
# 	then
# 		s=0 ;
# 	else
# 		echo ">>>$q" ;
# 	fi
# done
