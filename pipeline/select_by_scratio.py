#!/usr/bin/env python
#-*- encoding:utf-8 -*-


import sys, math, os

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def usage():
	print 'Usage: python ' + sys.argv[0] + ' sam_sorted_by_name prefix'
	print 'The file will be listed as prefix_1.fastq and prefix_2.fastq'
	sys.exit(-1)

# about CIGAR such as 4S37M60S will report right clipped point 
# Should we even report such things?
####################	
def parse_sc( cigar):
	shift = 0
	tmp = 0
	flag = 0
	n_clip = 0
	rl = 0
	ml = 0
	for ch in cigar:
		if ("M" == ch):
			shift = tmp
			flag = 0
			rl += tmp
			ml += tmp
			tmp = 0
		elif ("S" == ch):
			flag = 1
			n_clip+=1
			rl += tmp
			tmp = 0
		elif( ch.isalpha() ):
			rl += tmp
			tmp = 0
		else:
			tmp = tmp*10 + ord(ch)-ord('0')
	return rl, ml

####################	
def load_rc():
	rc_list = [
	'T', '-', 'G', '-', '-', '-', 'C', '-',  # A-H
	'-', '-', '-', '-', '-', 'N', '-', '-',  # I-P
	'-', '-', '-', 'A', '-', '-', '-', '-',  # Q-X
	'-', '-', # Y-Z
	'-', '-', '-', 'A', '-', '-',  # Q-X
	'T', '-', 'G', '-', '-', '-', 'C', '-',  # A-H
	'-', '-', '-', '-', '-', 'N', '-', '-',  # I-P
	'-', '-', '-', 'A', '-', '-', '-', '-',  # Q-X
	'-', '-' # Y-Z
			]
	return rc_list
####################	
def make_rc( raw_str, rc_list ):
	tmp_str= ""
	limit = len(raw_str)
	for i in range(0, limit):
		id = ord(raw_str[i])
		if 65 <= id and id <= 122:
			#print str(id-65) + " " + str(len(rc_list))
			tmp_str += rc_list[ id - 65]
		else:
			tmp_str += '-'
	return tmp_str
####################	
def main():
	args = sys.argv[1:]
	if len(args) != 2:
		usage()

	rc_list = load_rc()

	count    = 0
	n_orphan = 0
	r_list = []
	orphan_sw = open( sys.argv[2] + ".orphan", 'w' )
	sw_1 = open( sys.argv[2] + "_1.fastq", 'w' )
	sw_2 = open( sys.argv[2] + "_2.fastq", 'w' )
	sr = open( sys.argv[1], 'r' )
	id = ""
	r1, m1, r2, m2, flag1, flag2 = 0, 0, 0, 0, 0, 0
	seq1, q1, seq2, q2 = "", "", "", ""
	for line in sr:
		if ("@" == line[0]):
			continue
		t_list = line.split("\t")
		if ( t_list[0] != id ):
			if ( 0 < len(seq1) and 0 < len(seq2)):
				if 0 == r1 + r2:
					orphan_sw.write("@" + id + "/1\n" + seq1+ "\n+\n" + q1 + "\n")
					orphan_sw.write("@" + id + "/2\n" + seq2+ "\n+\n" + q2 + "\n")
					n_orphan +=1
				elif ( not (0x2 & flag1) or 0.99 > (m1+m2)*1.0/(r1+r2)):
					sw_1.write("@" + id + "/1\n" + seq1+ "\n+\n" + q1 + "\n")
					sw_2.write("@" + id + "/2\n" + seq2+ "\n+\n" + q2 + "\n")
			id = t_list[0]
			r1, m1, r2, m2, flag1, flag2 = 0, 0, 0, 0, 0, 0
			seq1, q1, seq2, q2 = "", "", "", ""
			count +=1

		flag = int(t_list[1])
		# mate 1
		if ( 256 > flag and (0x40 & flag)):
			flag1 = flag
			r1, m1 = parse_sc( t_list[5] )
			if ( 0x10 & flag):
				seq1 = make_rc(t_list[9], rc_list)
				q1   = t_list[10][::-1]
			else:
				seq1 = t_list[9] 
				q1   = t_list[10]
		# mate 2
		if ( 256 > flag and (0x80 & flag)):
			flag2 = flag
			r2, m2 = parse_sc( t_list[5] )
			if ( 0x10 & flag):
				seq2 = make_rc(t_list[9], rc_list)
				q2   = t_list[10][::-1]
			else:
				seq2 = t_list[9] 
				q2   = t_list[10]
			
		count +=1
	if ( 0 < len(seq1) and 0 < len(seq2)):
		if 0 == r1 + r2:
			orphan_sw.write("@" + id + "/1\n" + seq1+ "\n+\n" + q1 + "\n")
			orphan_sw.write("@" + id + "/2\n" + seq2+ "\n+\n" + q2 + "\n")
			n_orphan +=1
		elif ( not (0x2 & flag1) or 0.99 > (m1+m2)*1.0/(r1+r2)):
			sw_1.write("@" + id + "/1\n" + seq1+ "\n+\n" + q1 + "\n")
			sw_2.write("@" + id + "/2\n" + seq2+ "\n+\n" + q2 + "\n")
	print str(count) + " reads with " + str( n_orphan ) + " orphans."
	sr.close()
	orphan_sw.close()
	sw_1.close()
	sw_2.close()


if __name__ == "__main__":
    sys.exit(main())
