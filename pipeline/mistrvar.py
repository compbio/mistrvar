#!/usr/bin/env python
#786 
from __future__ import print_function

import os, sys, errno, argparse, subprocess, fnmatch, configparser, stat, math

# try:
#     from configparser import ConfigParser
# except ImportError:
#     from ConfigParser import SafeConfigParser as ConfigParser
#############################################################################################
# Class for colored texts and binary path
class bcolors:
	HEADER    = '\033[95m'
	OKBLUE    = '\033[94m'
	OKGREEN   = '\033[92m'
	WARNING   = '\033[93m'
	FAIL      = '\033[91m'
	ENDC      = '\033[0m'
	BOLD      = '\033[1m'
	UNDERLINE = '\033[4m'

class pipeline:
	mistrvar = os.path.dirname(os.path.realpath(__file__)) + "/mistrvar.py"
	sniper   = os.path.dirname(os.path.realpath(__file__)) + "/sniper"
	mrsfast  = os.path.dirname(os.path.realpath(__file__)) + "/mrsfast"
	workdir  = os.path.dirname(os.path.realpath(__file__))
	# example usage for help
	example  = "\tTo create a new project: specify (1) project name, (2) reference genomes and (3) input sequences (either --alignment, --fastq, or --mrsfast-best-search)\n"
	example += "\n\t--starting from a sam or bam file\n"
	example += "\t$ ./mistrvar.py -p my_project -r ref.fa --files alignment=my.sam\n"
	example += "\n\t--starting from a gzipped fastq file\n"
	example += "\t$ ./mistrvar.py -p my_project -r ref.fa --files fastq=my.fastq.gz\n"
	example += "\n\t--starting from a remapping result\n"
	example += "\t$ ./mistrvar.py -p my_project -r ref.fa --files mrsfast-best-search=my.best.sam\n"
	example += "\n\t--starting with a masked file\n"
	example += "\t$ ./mistrvar.py -p my_project -r ref.fa -m my-mask.txt  --files alignment=my.sam\n"	
	example += "\n\n\tTo resume a project, just type project folder and mistrvar.py will automatically resume from the previous stages:\n"
	example += "\t$ ./mistrvar.py -p my_project\n"
	example += "\t$ ./mistrvar.py -p /home/this/is/my/folder/project\n"


#############################################################################################
# Default values will be set up later in check_proj_preq
def command_line_process():
	parser = argparse.ArgumentParser(
		description='MiStrVar: Micro Structural Variant Caller',
		usage = pipeline.example,
		formatter_class=argparse.RawTextHelpFormatter
	)
	parser.add_argument('--project','-p',
		required=True,
		metavar='project',
		help='The name of the project. MiStrVar creates the folder if it does not exist'
	)
	parser.add_argument('--reference','-r',
		metavar='reference',
		help='The path to the reference genome that should be used for analysis'
	)
	parser.add_argument('--max-contig','-c',
		type=int,
		metavar='max_contig',
		help="Maximum size of assembled contigs. (default: 400)",
	)
	parser.add_argument('--local-assembly','-l',
		type=int,
		metavar='local_assembly',
		help="Strategy for local assembly. 1 for greedy-fashion assembly, 0 for overlap-layout assembly.  (default: 0, overlap-layout)",
	)
	parser.add_argument('--sv-type','-t',
		type=int,
		metavar='sv_type',
		help="SV Types considered in detection algorithm. 0 for inversion-only; 1 for duplication-only; 2 for all microSVs. (default: 2, all events)",
	)
	parser.add_argument('--ref-flank','-k',
		type=int,
		metavar='ref_flank',
		help="Length of reference flanking region in local assembly; Suggested to be at least twice fragment length. (default: 1000)",
	)
	parser.add_argument('--max-error','-e',
		type=int,
		metavar='max_error',
		help="Maximum number of mismatches within the structural variant. (default: 0)",
	)
	parser.add_argument('--min-support','-s',
		type=int,
		metavar='min_support',
		help="Support threshold (number of reads per base) for the predicted structural variants. (default: 5)",
	)
	parser.add_argument('--contig-identity','-i',
		type=int,
		metavar='contig_identity',
		help='Identity threshold (1..100) for an assembled contig to be considered for further analysis. (default: 85)',
	)
	parser.add_argument('--final-identity','-g',
		type=int,
		metavar='final_identity',
		help="Identity threshold (1..100) for the final alignment allowing for structural variants. (default: 95)",
	)	
	parser.add_argument('--mask-file', '-m',
		help='The coordinates provided in this file will be masked from the reference genome.'
	)
	parser.add_argument('--invert-masker',
		action='store_true',
		help='The provided coordinates will be masked in the reference genome and ignored in mapping step. (dafault: False)',
	)
	parser.add_argument('--mrsfast-index-ws',
		metavar='window_size',
		help='Window size used by mrsFAST-Ultra for indexing the reference genome. (default: 12)',
	)
	parser.add_argument('--mrsfast-crop',
		metavar='mrsfast_crop',
		help='Crop all reads to the specified length (default: disabled)',
	)
	parser.add_argument('--mrsfast-errors',
		metavar='mrsfast_errors',
		help='Number of the errors used by mrsFAST-Ultra for mapping (default: 4)',
	)
	parser.add_argument('--mrsfast-threads',
		metavar='mrsfast_threads',
		help='Number of the threads used by mrsFAST-Ultra for mapping (default: 1)',
	)
	parser.add_argument('--mrsfast-cutoff',
		metavar='mrsfast_cutoff',
		help='Maximum number of mapping loci of anchor of an OEA. Anchor with higher mapping location will be ignored in microSV detection. 0 for considering all mapping locations (default: 1)',
	)
	parser.add_argument('--resume',
		nargs='?',
		const="sniper",
		help='Ignore existing progress and restart pipeline. Put sniper if you want to automatically resume from an previously killed task.',
	)
	parser.add_argument('--cluster',
		metavar='cluster',
		help='For input range x-y, report reads of cluster from x to y-1 and exit. Supported only in resume mode.',
	)
	parser.add_argument('--resume-force', '-f',
		action='store_true',
		help='Ignore existing files and restart the pipeline from stage specified in --resume.',
	)
	parser.add_argument('--num-worker',
		type=int,
		help='Number of independent prediction jobs which will be created. (default: 1)',
	)
	parser.add_argument('--range',
		help='Intervals of OEA clusters in partition to be analyzed.',
	)
	parser.add_argument('--worker-id',
		help='Specific worker ID that the user wants to run prediction in the last stage. (default:-1, will run on all workers)',
	)
	parser.add_argument('--mode',
		metavar='engine_mode',
		help='Type for running indepedent jobs: "normal", "sge", or "pbs". (default: normal).',
		default='normal'
	)
	parser.add_argument('--job-max-time',
		help='Max job running time in PBS file. Read documents before changing its value! (default: 6 hour.)',
		default='06:00:00'
	)
	parser.add_argument('--job-max-memory',
		help='Max job memory in PBS file. Read docuemnts before changing its value! (default: 16 GB.)',
		default='16G'
	)
	parser.add_argument('--files',
		metavar='files',
		nargs='+',
		type=str,
		default=[]
	)

	return parser

#############################################################################################
### get the system shell to run the command and pipe stdout to control file when log==True
def log( msg, output=True ):
	if output:
		print(msg, end='')
		sys.stdout.flush()
	
#############################################################################################
### get the system shell to run the command and pipe stdout to control file when log==True
def logln( msg, output=True ):
	if output:
		print(msg)
		sys.stdout.flush()

#############################################################################################
### get the system shell to run the command and pipe stdout to control file when log==True
def logOK():
	logln(bcolors.OKGREEN+"OK"+bcolors.ENDC)

#############################################################################################
### get the system shell to run the command and pipe stdout to control file when log==True
def logWAR():
	logln(bcolors.WARNING+"SKIPPING"+bcolors.ENDC)

#############################################################################################
### get the system shell to run the command and pipe stdout to control file when log==True
def logFAIL():
	logln(bcolors.FAIL+"FAILED"+bcolors.ENDC)

#############################################################################################
### get the system shell to run the command and pipe stdout to control file when log==True
def shell(msg, run_command, command, control_file='', success_file='', success_msg='', shell_log=True):
	
	if shell_log:
		log("{0}...".format(msg))

	if (not run_command):
		logWAR()
		return 
	if not shell_log or control_file == '':
		p = subprocess.Popen(command, shell=True)
	else:
		f = open(control_file, 'w')
		f.write("CMD:" + command + "\n")
		f.flush() # so commands will show before actual progress
		p = subprocess.Popen(command, shell=True, stdout=f, stderr=f)

	ret = p.wait()
	if shell_log and control_file != '':
		f.close()
	if ret != 0:
		logFAIL()
		raise subprocess.CalledProcessError(ret, command)

	elif success_file != '':
		with open (success_file, 'w') as suc_file:
			suc_file.write(success_msg)
		logOK()
#############################################################################################
### generate scripts to merge vcf, reads, and alignment when users run multiple workers
def generate_merge_script( config, num_worker, engine_mode ):
	appsdir  = os.path.dirname(os.path.realpath(__file__))
	worker_prefix   = "{0}/jobs/mistrvar".format( pipeline.workdir )
	output_prefix   = "{0}/{1}".format( pipeline.workdir, config.get("project", "name") )
	job_file 		=  "{0}/jobs.sh".format(pipeline.workdir )
	script_file   	= "{0}/run.sh".format(pipeline.workdir )

	if(engine_mode == "sge" or engine_mode == "pbs"):
		with open( pipeline.workdir + "/merge_vcf.sh", 'w' ) as f_script:
			f_script.write("#!/bin/bash\n")
			cmd = 'FILE={0}.vcf\nif [ -f $FILE ]; then rm -f ${{FILE}}; fi & touch ${{FILE}}\n'.format( output_prefix )
			cmd += 'cat {0}/header.vcf >> ${{FILE}}'.format( appsdir)
			f_script.write("{0}\n".format(cmd))
			for i in range( num_worker ):
				f_script.write("part_info={0}_{1}.vcf; if [ -f ${{part_info}} ]; then cat ${{part_info}} >> ${{FILE}}; else printf \"Missing File %s\\n\" ${{part_info}};fi\n".format(worker_prefix, i))
		f_script.close()
		with open( pipeline.workdir + "/merge_alignment.sh", 'w' ) as f_script:
			f_script.write("#!/bin/bash\n")
			cmd = 'FILE={0}.align\nif [ -f $FILE ]; then rm -f ${{FILE}}; fi & touch ${{FILE}}\n'.format( output_prefix )
			f_script.write("{0}\n".format(cmd))
			for i in range( num_worker ):
				f_script.write("part_info={0}_{1}; if [ -f ${{part_info}} ]; then cat ${{part_info}} >> ${{FILE}}; else printf \"Missing File %s\\n\" ${{part_info}};fi\n".format(worker_prefix, i))
		f_script.close()
		with open( pipeline.workdir + "/merge_sup.sh", 'w' ) as f_script:
			f_script.write("#!/bin/bash\n")
			cmd = 'FILE={0}.vcf.reads\nif [ -f $FILE ]; then rm -f ${{FILE}}; fi & touch ${{FILE}}\n'.format( output_prefix )
			f_script.write("{0}\n".format(cmd))
			for i in range( num_worker ):
				f_script.write("part_info={0}_{1}.vcf.reads; if [ -f ${{part_info}} ]; then cat ${{part_info}} >> ${{FILE}}; else printf \"Missing File %s\\n\" ${{part_info}};fi\n".format(worker_prefix, i))
		f_script.close()
	else:
		with open( script_file, 'w' ) as f_script:
			f_script.write("#!/bin/bash\n")
			cmd = "cat {0} | xargs -I CMD --max-procs={1} bash -c CMD\n".format(job_file, num_worker )
			cmd += 'FILE={0}.vcf\nif [ -f $FILE ]; then rm -f ${{FILE}}; fi & touch ${{FILE}}\n'.format( output_prefix )
			cmd += 'cat {0}/header.vcf >> ${{FILE}}'.format( appsdir)
			f_script.write("{0}\n".format(cmd))
			for i in range( num_worker ):
				f_script.write("part_info={0}_{1}.vcf; if [ -f ${{part_info}} ]; then cat ${{part_info}} >> ${{FILE}}; else printf \"Missing File %s\\n\" ${{part_info}};fi\n".format(worker_prefix, i))
			cmd = 'FILE={0}.align\nif [ -f $FILE ]; then rm -f ${{FILE}}; fi & touch ${{FILE}}\n'.format( output_prefix )
			f_script.write("{0}\n".format(cmd))
			for i in range( num_worker ):
				f_script.write("part_info={0}_{1}; if [ -f ${{part_info}} ]; then cat ${{part_info}} >> ${{FILE}}; else printf \"Missing File %s\\n\" ${{part_info}};fi\n".format(worker_prefix, i))
			cmd = 'FILE={0}.vcf.reads\nif [ -f $FILE ]; then rm -f ${{FILE}}; fi & touch ${{FILE}}\n'.format( output_prefix )
			f_script.write("{0}\n".format(cmd))
			for i in range( num_worker ):
				f_script.write("part_info={0}_{1}.vcf.reads; if [ -f ${{part_info}} ]; then cat ${{part_info}} >> ${{FILE}}; else printf \"Missing File %s\\n\" ${{part_info}};fi\n".format(worker_prefix, i))
		f_script.close()
		st = os.stat(script_file)
		os.chmod(script_file, st.st_mode | stat.S_IEXEC)

#############################################################################################
########## Clean stage file for resuming
def clean_state_worker( workdir, config):
	workdir = pipeline.workdir
	stage_dir = workdir + "/stage/"
	worker_stage_files = [f for f in os.listdir( stage_dir ) if (os.path.isfile( os.path.join( stage_dir , f)) and "11"==f[0:2])]
	for item in worker_stage_files:
		log(" + Removing " + item  + "...")
		os.remove( stage  + item)
		logOK()
		
#############################################################################################
########## Clean stage file for resuming
def clean_state( mode_index, workdir, config ):
	flag_clean = 0 # 1 only when we delete files due to changes in project.config
	workdir = pipeline.workdir	
	valid_state = [ '01.getfastq', '02.mask', '03.mrsfast-index', '04.mrsfast.best', '05.oea', '06.mrsfast', '07.sorted', '08.oeaunm', '09.sniper_part', '10.worker', 'normal']
	for i in range( mode_index, len(valid_state)):
		if ( 3==i and ""!=config.get("project","mrsfast-best-search")):
			continue
		if os.path.isfile(workdir + "/stage/" + valid_state[i] + ".finished" ):
		#try:
			if 0 == flag_clean:
				logln("Removing old files due to change in project.config")
				flag_clean = 1
			log(" + Removing " + valid_state[i] + ".finished...")
			os.remove( workdir + "/stage/" + valid_state[i] + ".finished" )
			logOK()
	# Clean all possible stages files of previous worker
	clean_state_worker( workdir, config)
	#except subprocess.CalledProcessError as e:
	#	print >>sys.stderr, "{0} failed with exit status {2} and message {1}".format(e.cmd, 'N/A', e.returncode)


#############################################################################################
###### Running commands for getfastq 
def getfastq(config ):
	msg           = "Extracting FASTQ from Alignment file"
	project_name  = config.get("project", "name")
	workdir		  = pipeline.workdir
	input_file    = "{0}/{1}".format(workdir, config.get("project","alignment"))
	output_file   = "{0}/{1}".format(workdir, config.get("project","fastq"))
	control_file  = "{0}/log/01.getfastq.log".format(workdir);
	complete_file = "{0}/stage/01.getfastq.finished".format(workdir);
	freeze_arg    = ""
	cmd           = pipeline.sniper + ' fastq {0} {1} {2}'.format( input_file, output_file, config.get("mrsfast", "errors") )
	cmd          += '; mv {1}.length {0}/log/getfastq.length'.format(workdir, output_file)
	run_cmd       = not (os.path.isfile(complete_file) )

	shell( msg, run_cmd , cmd, control_file, complete_file, freeze_arg)
	
#############################################################################################
###### Running commands for getfastq 
def mask(config):
	msg			  = "Masking Reference Genome"
	project_name  = config.get("project", "name")
	workdir		  = pipeline.workdir
	input_file    = "{0}/{1}".format(workdir, config.get("project","reference"))
	mask_file     = "{0}/{1}".format(workdir, config.get("sniper","mask-file"))
	output_file   = "{0}/{1}.masked".format(workdir, config.get("project","reference"))
	mask_mode     = "maski" if ( "True" == config.get("sniper","invert-masker") ) else "mask"
	if ("maski" == mask_mode):
		msg += " to Keep Only Regions Specified in File"

	control_file  = "{0}/log/02.mask.log".format(workdir);
	complete_file = "{0}/stage/02.mask.finished".format(workdir);
	freeze_arg    = "invert" if "True" == config.get("sniper","invert-masker")  else "mask"
	cmd           = pipeline.sniper +' {0} {1} {2} {3} 0'.format( mask_mode, mask_file, input_file, output_file )
	run_cmd       = not ( os.path.isfile(complete_file) and freeze_arg in open(complete_file).read()) 

	if ( run_cmd ):
		clean_state( 1, workdir, config )
	shell( msg, run_cmd, cmd, control_file, complete_file, freeze_arg)

#############################################################################################
###### Running commands for each mode 
def index(config):
	msg           = "Indexing the masked genome using mrsFAST-Ultra"
	project_name  = config.get("project", "name")
	workdir		  = pipeline.workdir
	input_file    = "{0}/{1}.masked".format(workdir, config.get("project", "reference"))
	control_file  = "{0}/log/03.mrsfast-index.log".format(workdir);
	complete_file = "{0}/stage/03.mrsfast-index.finished".format(workdir);
	freeze_arg    = "ws={0}".format(config.get("mrsfast", "window_size") )
	cmd           = pipeline.mrsfast + ' --index  {0} --ws {1}'.format(input_file, config.get("mrsfast","window_size") )
	run_cmd       = not ( os.path.isfile(complete_file) and freeze_arg in open(complete_file).read()) 

	if ( run_cmd ):
		clean_state( 2, workdir, config)
	shell( msg, run_cmd, cmd, control_file, complete_file, freeze_arg)
	
	#return msg, run_cmd, cmd, control_file, complete_file, freeze_arg
#############################################################################################
###### Running commands for each mode 
def mrsfast_best_search(config):
	msg           = "Mapping non-concordant reads using mrsFAST-Ultra"
	project_name  = config.get("project", "name")
	workdir		  = pipeline.workdir
	index_file    = "{0}/{1}.masked".format(workdir, config.get("project", "reference"))
	input_file    = "{0}/{1}".format(workdir, config.get("project","fastq"))
	output_file   = "{0}/mrsfast.best.sam".format(workdir)
	unmapped_file = "{0}/mrsfast.best.sam.unmap".format(workdir)
	crop 		  = "--crop " + config.get("mrsfast","crop") if config.get("mrsfast","crop") != "-1" else ""
	### automatically cropping detection
	rl            = 0
	rl_file       = "{0}/log/getfastq.length".format(workdir, output_file)
	if os.path.isfile( rl_file ):
		with open(rl_file) as f:
			rl= int( f.read().strip() )
		mrsfast_crop = int( config.get("mrsfast", "crop") )
		if -1 < mrsfast_crop and mrsfast_crop < rl:
			rl = mrsfast_crop
		crop      = "--crop " + str(rl)
	threads       = config.get("mrsfast", "threads")
	control_file  = "{0}/log/04.mrsfast.best.log".format(workdir);
	complete_file = "{0}/stage/04.mrsfast.best.finished".format(workdir);
	freeze_arg    = "ws={0}.crop={1}.error={2}.rl={3}".format(config.get("mrsfast", "window_size"), config.get("mrsfast","crop"), config.get("mrsfast", "errors"), rl)
	cmd           = pipeline.mrsfast +' --search  {0} {1} --threads {2} -e {3} --best --seq {4} --seqcomp -o {5} -u {6}'.format(index_file, crop, threads, config.get("mrsfast","errors"), input_file, output_file, unmapped_file )
	run_cmd       = not ( os.path.isfile(complete_file) and freeze_arg in open(complete_file).read()) 

	if ( run_cmd ):
		clean_state( 3, workdir, config )
	shell( msg, run_cmd, cmd, control_file, complete_file, freeze_arg)
	
	
#############################################################################################
###### Running commands for getfastq 
def oea(config):
	msg           = "Extracting OEA reads"
	project_name  = config.get("project", "name")
	workdir		  = pipeline.workdir
	input_file    = "{0}/mrsfast.best.sam".format(workdir)
	output_file   = "{0}/oea".format(workdir)
	control_file  = "{0}/log/05.oea.log".format(workdir);
	complete_file = "{0}/stage/05.oea.finished".format(workdir);
	freeze_arg    = ""
	cmd           = pipeline.sniper + ' oea {0} {1}'.format(  input_file, output_file )
	run_cmd       = not (os.path.isfile(complete_file) )
	shell( msg, run_cmd, cmd, control_file, complete_file, freeze_arg)
#############################################################################################
###### Running commands for each mode 
def mrsfast_search(config ):
	msg           = "Mapping OEA reads to get anchor locations using mrsFAST-Ultra"
	project_name  = config.get("project", "name")
	workdir		  = pipeline.workdir
	ref_file      = "{0}/{1}.masked".format(workdir, config.get("project", "reference"))
	input_file    = "{0}/oea.mapped.fq".format(workdir)
	output_file   = "{0}/mrsfast.sam".format(workdir)
	unmapped_file = "{0}/mrsfast.sam.unmap".format(workdir)
	threads       = config.get("mrsfast", "threads")
	cutoff        = config.get("mrsfast", "cutoff")
	
	control_file  = "{0}/log/06.mrsfast.log".format(workdir);
	complete_file = "{0}/stage/06.mrsfast.finished".format(workdir);
	freeze_arg    = "ws={0}.error={1}.cutoff={2}".format(config.get("mrsfast", "window_size"), config.get("mrsfast", "errors"), cutoff)
	cmd           = pipeline.mrsfast +' --search  {0} --threads {1} -e {2} --seq {3} -o {4} -u {5}'.format(ref_file, threads, config.get("mrsfast","errors"), input_file, output_file, unmapped_file )
	if ("0"!= cutoff):
		cmd += " -n {0}".format(cutoff )
	run_cmd       = not ( os.path.isfile(complete_file) and freeze_arg in open(complete_file).read()) 

	if ( run_cmd ):
		clean_state( 5, workdir, config )
	shell( msg, run_cmd, cmd, control_file, complete_file, freeze_arg)
#############################################################################################
###### Running commands for getfastq 
def sort(config ):
	msg           = "Sorting the mapped mates"
	project_name  = config.get("project", "name")
	workdir		  = pipeline.workdir
	input_file    = "{0}/mrsfast.sam".format( workdir )
	output_file   = "{0}/sorted.sam".format( workdir )
	control_file  = "{0}/log/07.sort.log".format( workdir );
	complete_file = "{0}/stage/07.sorted.finished".format( workdir );
	freeze_arg    = ""
	cmd           = pipeline.sniper +' sort {0} {1}'.format(  input_file, output_file )
	run_cmd       = not (os.path.isfile(complete_file) )

	shell( msg, run_cmd , cmd, control_file, complete_file, freeze_arg)
#############################################################################################
###### Running commands for getfastq 
def oeaunm(config ):
	msg           = "Extract unmapped mates of OEA reads"
	project_name  = config.get("project", "name")
	workdir		  = pipeline.workdir
	input_file    = "{0}/oea.unmapd.fq".format(workdir )
	output_file   = "{0}/unmapped".format(workdir )
	control_file  = "{0}/log/08.oeaunm.log".format(workdir);
	complete_file = "{0}/stage/08.oeaunm.finished".format(workdir);
	freeze_arg    = ""
	cmd           = pipeline.sniper + ' rm_unmap {0} {1}'.format(  input_file, output_file )
	run_cmd       = not (os.path.isfile(complete_file) )
	shell( msg, run_cmd, cmd, control_file, complete_file, freeze_arg)
#############################################################################################
###### Running commands for getfastq 
def sniper_part(config ):
	msg           = "Creating OEA clusters"
	project_name  = config.get("project", "name")
	workdir		  = pipeline.workdir
	input_file    = "{0}/sorted.sam".format(workdir)
	unmapped_file = "{0}/unmapped".format(workdir )
	output_file   = "{0}/sniper_part".format(workdir )
	control_file  = "{0}/log/09.sniper_part.log".format(workdir);
	complete_file = "{0}/stage/09.sniper_part.finished".format(workdir);
	freeze_arg    = "1000"
	cmd           = pipeline.sniper + ' partition {0} {1} {2} 1000'.format(  input_file, unmapped_file, output_file )
	run_cmd       = not (os.path.isfile(complete_file) )

	if ( run_cmd ):
		clean_state( 9, workdir, config )
	shell( msg, run_cmd, cmd, control_file, complete_file, freeze_arg)

#############################################################################################
########## Generate commands for each worker
def assign_worker(config):
	msg           = "Worker"
	project_name  = config.get("project", "name")
	workdir		  = pipeline.workdir
	#input_file    = "{0}/{1}.sniper_part".format(workdir, project_name )
	output_file   = "{0}/jobs.sh".format(workdir )
	control_file  = "{0}/log/10.worker.log".format(workdir);
	complete_file = "{0}/stage/10.worker.finished".format(workdir);

	with open('{0}/log/09.sniper_part.log'.format(workdir), 'r') as f:
		f.readline() # to skip the command part in log output
		nj = int(f.readline().strip())
	maxjobs = int( config.get("project", "num-worker") )
	if (maxjobs < 1):# sanity checking
		maxjobs = 1 
	
	freeze_arg    = "{0} {1}".format( nj, maxjobs ) 

	run_cmd       = not ( os.path.isfile(complete_file) and freeze_arg in open(complete_file).read())
	if (run_cmd):
		log("Assign jobs to independent workers...")
	
		if os.path.isfile(control_file ):
			os.remove( control_file)
		with open (control_file, 'a') as ctr_file:
			ctr_file.write("Find {0} paritions\n".format(nj))

		with open('{0}'.format(output_file), 'w') as fj:
			fj.write("#!/bin/bash\n")
		
		for i in range(maxjobs):

			st = int(math.ceil(float(nj) / float(maxjobs))) * i
			ed = min(nj, ((int(math.ceil(float(nj) / float(maxjobs)))) * (i + 1)))

			if st < nj:
				rng = '{0}-{1}'.format(st, ed)
				cmd = pipeline.mistrvar + " -p {0} -r {1} --range {2} --worker-id {3} --num-worker 1 --resume sniper \n".format( pipeline.workdir, config.get("project", "reference"), rng, i )
				if "sge" == config.get("sniper", "engine-mode"):
					worker_cmd = "qsub -cwd -V -b y -N {0}_{1} -l h_vmem={2} -l h_rt={3} -l h_stack=8M ".format( project_name, i, config.get("sniper", "job-memory"), config.get("sniper","job-time")) +  cmd;
				elif "pbs" == config.get("sniper", "engine-mode"):
					with open('{0}/pbs/{1}.pbs'.format(workdir, i), 'w') as fpbs:
						fpbs.write("#!/bin/bash\n")
						fpbs.write("#PBS -l nodes=1:ppn=1,vmem={0},walltime={1}\n".format( config.get("sniper", "job-memory"), config.get("sniper", "job-time")) )
						fpbs.write("#PBS -N {0}_{1}\n".format(project_name, i))
						fpbs.write("cd $PBS_O_WORKDIR\n")
						fpbs.write("{0}".format(cmd))
					worker_cmd = "qsub {0}/pbs/{1}.pbs".format(workdir,i)
				else: # local machine
					worker_cmd = cmd
		
				with open('{0}'.format(output_file), 'a') as fj:
					fj.write(worker_cmd)

				with open (control_file, 'a') as ctr_file:
					ctr_file.write("Finish worker {0}\n".format(i))
			else:
				worker_cmd = "touch {0}/jobs/mistrvar_{1}\n".format(workdir,i)
				worker_cmd += "touch {0}/jobs/mistrvar_{1}.reads\n".format(workdir,i)
				worker_cmd += "touch {0}/jobs/mistrvar_{1}.vcf\n".format(workdir,i)
				worker_cmd += "touch {0}/jobs/mistrvar_{1}.vcf.reads\n".format(workdir,i)
		
				with open('{0}'.format(output_file), 'a') as fj:
					fj.write(worker_cmd)

				with open (control_file, 'a') as ctr_file:
					ctr_file.write("Finish worker {0}\n".format(i))

		generate_merge_script( config, maxjobs, config.get("sniper", "engine-mode"))
		with open (complete_file, 'w') as suc_file:
			suc_file.write("{0}".format( freeze_arg ))
			logOK()
		# to re-run jobs
		clean_state( 10, workdir, config )

#############################################################################################
###### Running commands for getfastq 
def assemble(config ):
	msg           = "Extracting micorSVs for cluster range {0}".format(config.get("sniper", "range"))
	project_name  = config.get("project", "name")
	workdir		  = pipeline.workdir
	worker_id     = config.get("sniper", "worker-id")
	rng           = config.get("sniper", "range")
	ref_file    = "{0}/{1}.masked".format(workdir, config.get("project", "reference") )
	unmapped_file = "{0}/unmapped".format(workdir)
	output_prefix   = "{0}/jobs/mistrvar_{1}".format(workdir, worker_id )
	control_file  = "{0}/log/11.sniper.{1}.log".format(workdir, worker_id);
	complete_file = "{0}/stage/11.sniper.{1}.finished".format(workdir, worker_id);
	freeze_arg    = "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}".format(worker_id, rng, config.get("sniper", "max-contig"), config.get("sniper", "max-error"), config.get("sniper", "min-support"), config.get("sniper", "contig-identity"),config.get("sniper", "final-identity"),config.get("sniper", "ref-flank"), config.get("sniper", "local-assembly") )
	cmd           = pipeline.sniper + " assemble {0}/sniper_part {1} {2} {3}.vcf {3} {4} {5} {6} {7} 125 {8} {9} {10} {11}".format(workdir, ref_file, rng, output_prefix, config.get("sniper", "sv-type"),
	config.get("sniper", "max-contig"), config.get("sniper", "max-error"), config.get("sniper", "min-support"), config.get("sniper", "contig-identity"),config.get("sniper", "final-identity"), 
	config.get("sniper", "ref-flank"), config.get("sniper", "local-assembly") )
	run_cmd       = not ( os.path.isfile(complete_file) and freeze_arg in open(complete_file).read()) 
	shell( msg, run_cmd, cmd, control_file, complete_file, freeze_arg)

#############################################################################################
###### Running commands for extracting clusters 
def output_cluster(config, c_range ):
	msg           = "Extracting Clusters for range {0}".format(c_range)
	workdir		  = pipeline.workdir
	control_file  = "{0}/log/output_cluster.log".format(workdir);
	cmd           = pipeline.sniper + " get_cluster {0}/sniper_part {1}".format(workdir, c_range)
	shell( msg, True, cmd, control_file, '', '')
#############################################################################################
###### Running commands for each mode 
def run_command(config, force=False):

	getfastq(config)
	mask(config)
	index(config)
	mrsfast_best_search(config)
	oea(config)
	mrsfast_search(config)
	sort(config)
	oeaunm(config)
	sniper_part(config)
	assign_worker(config)
	engine_mode = config.get("sniper", "engine-mode")
	if ( ( "pbs" !=  engine_mode) and ( ( "sge" ) != engine_mode) ):
		num_parallel = config.get("project", "num-worker")
		msg = "Running prediction with {0} parallel task(s)".format(num_parallel)
		cmd = pipeline.workdir+ "/run.sh" 
		freeze_arg    = "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}".format(config.get("sniper", "max-contig"), config.get("sniper", "max-error"), config.get("sniper", "min-support"), config.get("sniper", "contig-identity"),config.get("sniper", "final-identity"),config.get("sniper", "ref-flank"), config.get("sniper", "local-assembly") )
		control_file  = "{0}/log/normal.log".format( pipeline.workdir )
		complete_file = "{0}/stage/normal.finished".format( pipeline.workdir)
		run_cmd       = not ( os.path.isfile(complete_file) and freeze_arg in open(complete_file).read()) 
		shell( msg, run_cmd, cmd, control_file, complete_file, freeze_arg)

#############################################################################################
def mkdir_p(path):
	try:
		os.makedirs(path)
	except OSError as e:
		if e.errno == errno.EEXIST and os.path.isdir(path):
			print("[ERROR] The project folder exists. Please run in resume mode or delete the project folder to re-run from scartch")
			exit(1);

#############################################################################################
######### link the absolute path of src in dest with identical filename
def symlink(src, dest):
	if src == '':
		return
	if not os.path.isfile(src):
		print("[ERROR] Input file " + src + " does not exist")
		exit(1)
	basename = os.path.basename(src)
	dest=os.path.abspath(dest)+'/'+basename
	#abs_src= os.path.abspath(src) + "/" + basename
	#os.symlink(src, dest)
	os.symlink(os.path.abspath(src), dest)

#############################################################################################
######### link the absolute path of src in dest with new filename
def symlink_name(src, dest, filename ):
	if src == '':
		return
	if not os.path.isfile(src):
		print("[ERROR] Input file " + src + " does not exist")
		exit(1)
	basename = os.path.basename(filename)
	dest=os.path.abspath(dest)+'/'+basename
	#abs_src= os.path.abspath(src) + "/" + basename
	#os.symlink(src, dest)
	os.symlink(os.path.abspath(src), dest)
#############################################################################################
##########	Make sure the project is successfully built
def is_exec(f):
	return os.path.isfile(f) and os.access(f, os.X_OK)

def check_binary_preq():
	execs = ['mrsfast', 'sniper']
	log( "Checking binary pre-requisites... ")
	for exe in execs:
		exe = os.path.dirname(os.path.realpath(__file__)) + "/" + exe
		if not is_exec(exe):
			print("[ERROR] File " + exe + " cannot be executed. Please use 'make' to build the required binaries.")
			logFAIL()
			logln ("File {0} cannot be executed. Please use 'make' to build the required binaries.".format(exe) )
			exit(1)
		
	logOK()

#############################################################################################
def resume_state_help():
	print("\nMiStrVar supports the following resume states:")
	print("\tgetfastq: extract fastq file from given alignment")
	print("\tmask: masking reference genome")
	print("\tmrsfast-index: indexing reference genome for further mapping")
	print("\tmrsfast-best-search: mapping candidate reads")
	print("\toea: extract oea reads")
	print("\tmrsfast-search: mapping oea reads to collect anchor locations")
	print("\tsort: sorting reads according to anchor locations")
	print("\toeaunm: extract unmapped mates for predicting microSVs")
	print("\tsniper_part: cluster unmapped reads according to anchor locations")
	print("\tnum-worker: generate jobs for parallel processing")
	print("\tsniper: output micorSVs")
	print("\nNOTE\tIf you want to automatically resume a killed job, just type --resume")

#############################################################################################
# Checking if necessary files are provided for a NEW project.
# reference sequence
# at least sam or fastq needs to exist
# masking file: if not provided, created a empty file
def check_input_preq( config ):
	workdir = pipeline.workdir

	if (None == config.get("project", "reference") ) or (not os.path.isfile( config.get("project", "reference") ) ):
		logFAIL()
		logln("Reference Genome {0} does not exist. Please provide valid name or path.".format( config.get("project", "reference") ))
		exit(1)
	
	if ("" !=  config.get("sniper", "mask-file") and  not os.path.isfile( config.get("sniper", "mask-file") )):
		logFAIL()
		logln("Invalid mask file {0}. Please provide a correct path.".format( os.path.abspath( config.get("sniper", "mask-file")) ))
		exit(1)

	inp = False # detect any valid input file or not
	if "" != config.get("project", "mrsfast-best-search"):
		inp = True
		if not os.path.isfile(config.get("project", "mrsfast-best-search")):
			logFAIL()
			logln("mrsFAST remapping file {0} does not exist. Please provide a correct path.".format( config.get("project", "mrsfast-best-search")))
			exit(1)
	
	if "" != config.get("project", "fastq"):
		if (inp) :
			logFAIL()
			logln("Please provide either alignment file, fastq file, or best-sesarch file.")
			exit(1)
		else:
			inp = True
		if not os.path.isfile(config.get("project", "fastq")):
			logFAIL()
			logln("Fastq file {0} does not exist. Please provide a correct path.".format( config.get("project", "fastq")))
			exit(1)
	
	if "" != config.get("project", "alignment"):
		if (inp) :
			logFAIL()
			logln("Please provide either alignment file, fastq file, or best-sesarch file.")
			exit(1)
		else:
			inp = True
		if not os.path.isfile(config.get("project", "alignment")):
			logFAIL()
			logln("Alignment file {0} does not exist. Please provide a correct path.".format( config.get("project", "alignment")))
			exit(1)
	
	if not inp:
		logFAIL()
		logln("Please provide either alignment file, fastq file, or best-sesarch file.")
		exit(1)

#############################################################################################
def get_input_file(config, args_files):
	for i in args_files:
		i = i.split('=')
		if i[0]=="alignment": 
			config.set("project","alignment",i[1])
			#start_from_fastq = 0 # start from alignment
		elif i[0]=='fastq':
			config.set("project","fastq",i[1])
			#start_from_fastq = 1 # start from fastq
		elif i[0]=='mask':
			config.set("project",'mask',i[1])
			#start_from_fastq = 1 # start from fastq
		elif i[0]=='mrsfast-index':
			config.set("project",'mrsfast-index',i[1])
			#start_from_fastq = 1 # start from fastq
		elif i[0]=='mrsfast-best-search':
			config.set("project",'mrsfast-best-search',i[1])
		#elif i[0]=='oea':
		#	config.set("project",'oea',i[1])
		#elif i[0]=='mrsfast-search':
		#	config.set("project",'mrsfast-search',i[1])
		#elif i[0]=='sort':
		#	config.set("project",'sort',i[1])
		#elif i[0]=='oeanum':
		#	config.set("project",'oeanum',i[1])
	return config

#############################################################################################
########## Initialze mrsfast parameters for before creating project folder
def initialize_config_mrsfast( config, args):
	config.add_section("mrsfast")
	config.set("mrsfast", "window_size", str( args.mrsfast_index_ws ) if args.mrsfast_index_ws != None else  "12")
	config.set("mrsfast", "threads", str( args.mrsfast_threads ) if args.mrsfast_threads != None else  "1")
	config.set("mrsfast", "crop", str( args.mrsfast_crop ) if args.mrsfast_crop != None else  "-1")
	config.set("mrsfast", "errors", str( args.mrsfast_errors ) if args.mrsfast_errors != None else  "4")
	config.set("mrsfast", "cutoff", str( args.mrsfast_cutoff ) if args.mrsfast_cutoff != None else  "50")
	return config

#############################################################################################
########## Initialze sniper parameters for before creating project folder
def initialize_config_sniper( config, args):
	workdir = pipeline.workdir
	config.add_section("sniper")
	config.set("sniper", "max-contig", str( args.max_contig ) if args.max_contig != None else  "400")
	config.set("sniper", "local-assembly", str( args.local_assembly ) if args.local_assembly != None else  "0")
	config.set("sniper", "sv-type", str( args.sv_type ) if args.sv_type != None else  "2")
	config.set("sniper", "ref-flank", str( args.ref_flank ) if args.ref_flank != None else  "1000")
	config.set("sniper", "max-error", str( args.max_error ) if args.max_error != None else "1")
	config.set("sniper", "min-support", str( args.min_support ) if args.min_support !=None else "5")
	config.set("sniper", "contig-identity", str( args.contig_identity)  if args.contig_identity !=None else "85")
	config.set("sniper", "final-identity", str( args.final_identity ) if args.final_identity !=None else "95")
	config.set("sniper", "mask-file", args.mask_file if args.mask_file != None else "" )
	config.set("sniper", "engine-mode",args.mode if args.mode != None else "normal")
	config.set("sniper", "invert-masker", str(args.invert_masker) if args.invert_masker !=None else "False")
	config.set("sniper", "range", str( args.range ) if args.range !=None else "-1" )
	config.set("sniper", "worker-id", str(args.worker_id) if args.worker_id != None else "-1")
	config.set("sniper", "job-time", args.job_max_time if args.job_max_time != None else "06:00:00")
	config.set("sniper", "job-memory",args.job_max_memory if args.job_max_memory != None else "16G")
	return config

#############################################################################################
def check_project_preq():
	args = command_line_process().parse_args()
	config = configparser.ConfigParser()
	
	project_name = os.path.basename(os.path.normpath(args.project))
	pipeline.workdir = os.path.abspath(args.project)
	workdir = pipeline.workdir

	print("=============================================")
	print("Project Name      : "+bcolors.OKGREEN+project_name+bcolors.ENDC)
	print("Working Directory : "+bcolors.OKGREEN+workdir+bcolors.ENDC)
	print("=============================================")
	
	# Check if users want to resume the project
	if ( os.path.isdir( workdir)):
		log ("Checking the project pre-requisites... ")
		if ( None == args.resume):
			logFAIL()
			logln("MiStrVar can not overwrite an existing project. Please add --resume or change project name.")
			exit(1)
			
		if not os.path.isfile( workdir + "/project.config"):
			logFAIL()
			logln("NO config settings found. Please remove and re-create the project.")
			exit(1)
		logOK()
		
		log ("Loading the config file... ")
		config.read(workdir + '/project.config');
		# update range  and worker id for assemble stage in SGE and PBS
		config.set("sniper","range", str( args.range ) if args.range !=None else "-1" )
		config.set("sniper","worker-id", str( args.worker_id ) if args.worker_id !=None else "-1" )
		logOK()
		# Entering cluster extraction mode
		if ( args.cluster != None ):
			output_cluster( config, args.cluster )	
			logOK()
			exit(0)

	# Start a new project
	else:
		log("Creating a new project folder...")
		# set up main project parameter
		config.add_section("project")
		config.set("project", "name", project_name)
		config.set("project", "reference", args.reference)
		config.set("project", "num-worker", str(args.num_worker) if args.num_worker != None else "1" )
		config.set("project", "alignment",'')
		config.set("project", "fastq",'')
		config.set("project", "mrsfast-best-search",'')
		config = get_input_file( config, args.files)

		# Parameters for other parts in the pipeline
		initialize_config_mrsfast(config, args)
		initialize_config_sniper(config, args)
		
		#validating required files according to mode
		check_input_preq(config)
		
		# creating project folder
		mkdir_p(workdir)
		mkdir_p(workdir +'/jobs');
		mkdir_p(workdir +'/log');
		mkdir_p(workdir +'/pbs');
		mkdir_p(workdir +'/stage');

		# linking reference genome
		symlink(config.get("project", "reference"), workdir)
		config.set("project", "reference", os.path.basename(config.get("project", "reference")))

		if ("" != config.get("sniper", "mask-file")):
			symlink(config.get("sniper", "mask-file"),  workdir)
			config.set("sniper", "mask-file", os.path.basename(config.get("sniper", "mask-file")))

		# Exactly one non-empty path for input sources. set up files based on input source
		if ( "" != config.get("project", "alignment")):
			symlink(config.get("project", "alignment"), workdir)
			config.set("project", "alignment", os.path.basename(config.get("project", "alignment")) )
			config.set("project", "fastq",  project_name + ".fastq.gz" )
			
		elif( "" != config.get("project", "fastq")):
			symlink(config.get("project", "fastq"), workdir)
			config.set("project", "fastq", os.path.basename(config.get("project", "fastq")))
		elif( "" != config.get("project", "mrsfast-best-search")):
			symlink_name(config.get("project", "mrsfast-best-search"), workdir, "mrsfast.best.sam")
			config.set("project", "mrsfast-best-search", os.path.basename(config.get("project", "mrsfast-best-search")))
			# Make sure mrsfast-best-search will not start when giving best mapping file
			with open("{0}/stage/04.mrsfast.best.finished".format(workdir), 'w') as complete_file:
				complete_file.write("ws={0}.error={1}\n".format(config.get("mrsfast", "window_size"), config.get("mrsfast", "errors")) )
		#else:
		#	loglng("[ERROR] Multiple input data sources. Check --files parameters before re-run")
		#	exit(1)

		# creating config in folder
		with open ( workdir +"/project.config", "w") as configFile:
			config.write(configFile)
		logOK()


	if ("" == config.get("sniper", "mask-file")):
		file = open ("{0}/mask.txt".format(workdir),'w')
		file.close()
		config.set("sniper","mask-file", "mask.txt")

	# set up stage files. Note that exactly one of three files are non-empty now
	if ("" == config.get("project", "alignment") ):
		file = open ("{0}/stage/01.getfastq.finished".format(workdir), 'w')
		file.close()

	return config
#############################################################################################
def main():
	config = check_project_preq()
	check_binary_preq()

	resume_state="sniper"

	try:
		if config.get("sniper", "range") == '-1':
			run_command(config)
		elif resume_state == "sniper":
			assemble(config)
		else:
			raise Exception('Invalid mode selected: ' + mode)
	except subprocess.CalledProcessError as e:
		print(e.cmd + " failed with exit status " + str(e.returncode) + " and message " + str(e.output), file=sys.stderr)

#############################################################################################
if __name__ == "__main__":
    sys.exit(main())

