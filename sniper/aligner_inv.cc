#include <iostream>
#include <fstream>
#include <string>
#include <unordered_map>
#include <algorithm>
#include <limits>
#include <cctype>
#include <math.h>
#include <pthread.h>
#include <unistd.h>
#include <sstream>
#include <sys/time.h>
#include "aligner_inv.h"

#define cost(A,B,C,D) (A==B) ? C : D
#define maxM(A,B) (((A) > (B)) ? (A) : (B))
#define minM(A,B) (((A) < (B)) ? (A) : (B))

using namespace std;

aligner_inv::aligner_inv(int M, 
						int p,
						double m,
						int s, 
						int e, 
						int l, 
						int L, 
						int I, 
						int inv, 
						int gstart, 
						int gext, 
						int miss, 
						int match,
						int mode, 
						bool dyn){

	dynamic = dyn;
	min_identity = m;
	min_support = s;
	max_errors = e;
	adj_max_errors = (max_errors*2)*mis;
	minLen = l;
	maxLen = L;
	maxIndel = I;
	invPen = inv;
	gapStart = gstart;
	gapExt = gext;
	mis = miss; 
	mat = match;
	maxSeqLen = M;
	num_threads = p;
	MODE = mode;
	gapStartExt = gapStart + gapExt;

	int L1 = maxSeqLen;
	int L2 = maxSeqLen;

	int availThreads = sysconf( _SC_NPROCESSORS_ONLN );

	if(num_threads > availThreads){
		cerr << "System cannot support " << num_threads << " threads, using " << availThreads << endl;
		num_threads = availThreads;
	}

	threads = new pthread_t[num_threads];
	curThID = 0;

	/*w = new int*** [num_threads];
	gs = new int*** [num_threads];
	gt = new int*** [num_threads];

	for(int i = 0; i < num_threads; i++){*/

		w = new int** [3];
		gs = new int** [3];
		gt = new int** [3];

		for(int j = 0; j < 3; j ++ ){

			w[j] = new int* [L2+1];
			gs[j] = new int* [L2+1];
			gt[j] = new int* [L2+1];

			for(int k = 0; k <= L2; k++){

				w[j][k] = new int [L1+1];
				gs[j][k] = new int [L1+1];
				gt[j][k] = new int [L1+1];
			}
		}
	//}

	if(MODE == MODE_INV_ONLY || MODE == MODE_BOTH){

		/*wI = new int*** [num_threads];
		gsI = new int*** [num_threads];
		gtI = new int*** [num_threads];

		for(int i = 0; i < num_threads; i++){*/

			wI = new int** [3];
			gsI = new int** [3];
			gtI = new int** [3];

			for(int j = 0; j < 3; j ++ ){

				wI[j] = new int* [L2+1];
				gsI[j] = new int* [L2+1];
				gtI[j] = new int* [L2+1];

				for(int k = 0; k <= L2; k++){

					wI[j][k] = new int [L1+1];
					gsI[j][k] = new int [L1+1];
					gtI[j][k] = new int [L1+1];
				}
			}
		//}

		//invs = new inversion** [num_threads];
		//for(int i = 0; i < num_threads; i++){
			invs = new inversion* [L2+1];
			for(int j = 0; j <= L2; j++){
				invs[j] = new inversion [L1+1];
			}
		//}
	}

	if(MODE == MODE_DUP_ONLY || MODE == MODE_BOTH){

		//dups = new duplication** [num_threads];
		//for(int i = 0; i < num_threads; i++){
			dups = new duplication* [L2+1];
			dup_errors = new int* [L2+1];
			for(int j = 0; j <= L2; j++){
				dups[j] = new duplication [L1+1];
				dup_errors[j] = new int [L2+1];
			}


		//}
	}

	opt = new int[L2+1];

	init(maxSeqLen, maxSeqLen, true);
}

aligner_inv::~aligner_inv(){

	int L1 = maxSeqLen;
	int L2 = maxSeqLen;

	delete threads;

	//for(int i = 0; i < num_threads; i++){
		for(int j = 0; j < 3; j ++ ){
			for(int k = 0; k <= L2; k++ ){
				delete w[j][k];
				delete gs[j][k];
				delete gt[j][k];
			}
			delete[] w[j];
			delete[] gs[j];
			delete[] gt[j];
		}
	//}
	delete[] w;
	delete[] gs;
	delete[] gt;

	if(MODE == MODE_INV_ONLY || MODE == MODE_BOTH){
		//for(int i = 0; i < num_threads; i++){
			for(int j = 0; j < 3; j ++ ){
				for(int k = 0; k <= L2; k++ ){
					delete wI[j][k];
					delete gsI[j][k];
					delete gtI[j][k];
				}
				delete[] wI[j];
				delete[] gsI[j];
				delete[] gtI[j];
			}
		//}
		delete[] wI;
		delete[] gsI;
		delete[] gtI;

		//for(int i = 0; i < num_threads; i++){
			for(int j = 0; j <= L2; j++ ){
				delete invs[j];
			}
			delete[] invs;
		//}
		//delete[] invs;
	}

	if(MODE == MODE_DUP_ONLY || MODE == MODE_BOTH){
		//for(int i = 0; i < num_threads; i++){
			for(int j = 0; j <= L2; j++ ){
				delete dups[j];
				delete dup_errors[j];
			}
			delete[] dups;
			delete[] dup_errors;
		//}
		//delete[] dups;
	}

	delete[] opt;
}

void	aligner_inv::init(int L1, int L2, bool global){

	//for(int th = 0; th < num_threads; th++){
		for(int x = 0; x < 3; x++){
			w[x][0][0] = 0;
			gs[x][0][0] = (x != 2) ? 0 : MIN_INT;//0;
			gt[x][0][0] = (x != 2) ? 0 : MIN_INT;//0;

			for(int j = 1; j<= L1; j++){
				w[x][0][j] = /*gapStart + */(j*gapExt);
				gs[x][0][j] = MIN_INT;
				gt[x][0][j] = (x != 2) ? (j*gapExt) : MIN_INT;///* gapStart +*/ (j*gapExt);
			}
			for(int i = 1; i <= L2; i++){
				w[x][i][0] =	/*gapStart +*/ (i*gapExt);
				gs[x][i][0] = (x != 2) ? (i*gapExt) : MIN_INT;///*gapStart +*/ (i*gapExt);
				gt[x][i][0] = MIN_INT;		 
			}
		}

		if(MODE == MODE_INV_ONLY || MODE == MODE_BOTH){
			for(int x = 0; x < 3; x ++ ){
				wI[x][0][0] = 0;
				gsI[x][0][0] = 0;
				gtI[x][0][0] = 0;
			}

			for(int j = 1; j<= L1; j++){
				wI[NG][0][j] =	gapStart + (j*gapExt);
				gsI[NG][0][j] = MIN_INT;
				gtI[NG][0][j] = gapStart + (j*gapExt);
				wI[GS][0][j] =	gapStart + (j*gapExt);
				gsI[GS][0][j] = MIN_INT;
				gtI[GS][0][j] = gapStart + (j*gapExt);
				wI[GT][0][j] = (j*gapExt);
				gsI[GT][0][j] = MIN_INT;
				gtI[GT][0][j] = (j*gapExt);
			}
			for(int i = 1; i <= L2; i++){
				wI[NG][i][0] =	gapStart + (i*gapExt);
				gsI[NG][i][0] = gapStart + (i*gapExt);
				gtI[NG][i][0] = MIN_INT;
				wI[GS][i][0] =	(i*gapExt);
				gsI[GS][i][0] = (i*gapExt);
				gtI[GS][i][0] = MIN_INT;
				wI[GT][i][0] =	gapStart + (i*gapExt);
				gsI[GT][i][0] = gapStart + (i*gapExt);
				gtI[GT][i][0] = MIN_INT;
			}
			for(int i = 1; i <= L2; i++){
				for(int j = 1; j <= L1; j++){
					invs[i][j].score = MIN_INT;
				}
			}
		}

		if(MODE == MODE_DUP_ONLY || MODE == MODE_BOTH){
			for(int i = 1; i <= L2; i++){
				for(int j = 1; j <= L1; j++){
					dups[i][j].score = MIN_INT;
				}
				for(int ii = 1; ii <= L2; ii++){
					dup_errors[i][ii] = MIN_INT;
				}
			}
		}
	//}
}

int aligner_inv::max3(int a, int b, int c){
	return maxM( maxM(a,b) , c);
}

int aligner_inv::max4(int a, int b, int c, int d){
	return maxM( maxM(a,b) , maxM(c,d));
}

string aligner_inv::rev_comp(string s){

	string r;
	
	for(int i = 0; i < s.length(); i++){
		if(s[i] == 'A'){
			r += 'T';
		}
		else if(s[i] == 'T' || s[i] == 'U'){
			r += 'A';
		}
		else if(s[i] == 'C'){
			r += 'G';
		}
		else if(s[i] == 'G'){
			r += 'C';
		}
		else if(s[i] == 'M'){
			r += 'K';
		}
		else if(s[i] == 'K'){
			r += 'M';
		}
		else if(s[i] == 'R'){
			r += 'Y';
		}
		else if(s[i] == 'Y'){
			r += 'R';
		}
		else if(s[i] == 'B'){
			r += 'V';
		}
		else if(s[i] == 'D'){
			r += 'H';
		}
		else if(s[i] == 'H'){
			r += 'D';
		}
		else if(s[i] == 'V'){
			r += 'B';
		}
		else if(s[i] == 'W' || s[i] == 'S' || s[i] == 'N'){
			r += s[i];
		}
		else{
			cerr << "Unrecognized nucleotide = " << s[i] << ", skipping" << endl;
		}
	}

	reverse(r.begin(), r.end());

	return r;
}

void	aligner_inv::print_matrix(int ** F, string seq1, string seq2 ){
		int	L1 = seq1.length();
		int	L2 = seq2.length();

		cout << "    j   ";
		for( int j = 0; j < L1; j++ ){
				cout << seq1[j] << "   ";
		}
		cout << "\ni ";

		for( int i = 0; i <= L2; i++ ){
				if( i > 0 ){
						cout << seq2[i-1] << " ";
				}
				for( int j = 0; j <= L1; j++){
						cout.width(3);
						cout << F[i][j] << " ";
				}
				cout << endl;
		}
}

void	aligner_inv::print_matrix(bool ** F, string seq1, string seq2 ){
		int	L1 = seq1.length();
		int	L2 = seq2.length();

		cout << "        ";
		for( int j = 0; j < L1; j++ ){
				cout << seq1[j] << "   ";
		}
		cout << "\n	";

		for( int i = 0; i <= L2; i++ ){
				if( i > 0 ){
						cout << seq2[i-1] << " ";
				}
				for( int j = 0; j <= L1; j++){
						cout.width(3);
						cout << F[i][j] << " ";
				}
				cout << endl;
		}
}

void	aligner_inv::print_matrix(char ** F, string seq1, string seq2 ){
		int	L1 = seq1.length();
		int	L2 = seq2.length();

		cout << "        ";
		for( int j = 0; j < L1; j++ ){
				cout << seq1[j] << "   ";
		}
		cout << "\n	";

		for( int i = 0; i <= L2; i++ ){
				if( i > 0 ){
						cout << seq2[i-1] << " ";
				}
				for( int j = 0; j <= L1; j++){
						cout.width(3);
						cout << F[i][j] << " ";
				}
				cout << endl;
		}
}

//==================================
//Alignment functions
//==================================

inline void aligner_inv::global_align(string s, string t, int ** ab, int ** ga, int ** gb, int mIndel){

	int L1 = s.length();
	int L2 = t.length(); 
	int c;
	int i = 1, j = 1;
	int ii, jj;

	for(i = 1; ((abs(i-j) <= mIndel) && i <= L2); i++){
		ii = i-1;
		for(j = maxM(i-mIndel,1); ((abs(i-j) <= mIndel) && j <= L1); j++){
			jj = j-1;
			ga[i][j] = ((j-i) != mIndel) ? maxM(ga[ii][j] + gapExt, ab[ii][j] + gapStartExt) : MIN_INT;
			gb[i][j] = ((i-j) != mIndel) ? maxM(gb[i][jj] + gapExt, ab[i][jj] + gapStartExt) : MIN_INT;
			ab[i][j] = maxM(maxM(ab[ii][jj], ga[ii][jj]), gb[ii][jj]) + (cost(t[ii],s[jj],mat,mis));
		}
		j = maxM((i+1)-mIndel,1);
	}
}

void aligner_inv::traceback(int& i, 
								int& j, 
								int& cur,
								int& matches, 
								int& mismatches, 
								int& gaps, 
								string& s, 
								string& t, 
								string& seq1Al, 
								string& seq2Al, 
								string& align,
								int ** ab, 
								int ** ga, 
								int ** gb,
								bool inversion){

	if(cur == 0){

		if(i == 0){
			cur = 2;
			return;
		}
		else if(j == 0){
			cur = 1;
			return;
		}

		int c = cost(t[i-1],s[j-1],mat,mis);

		if(c == mat){
			seq1Al += s[j-1];
			align += ((inversion)?'X':'|'); 
			seq2Al += t[i-1];
			matches++;
		}
		else{
			seq1Al += s[j-1];
			align += ((inversion)?'x':' '); 
			seq2Al += t[i-1];
			mismatches++;
		}

		if(ab[i-1][j-1] + c != ab[i][j]){
			if(ga[i-1][j-1] + c == ab[i][j]){
				cur = 1;
			}
			else if(gb[i-1][j-1] + c == ab[i][j]){
				cur = 2;
			}
		}
		i--;	j--;
	}
	else if(cur == 1){
		seq1Al += '-';
		align += ((inversion)?'x':' '); 
		seq2Al += t[i-1];
		if (ab[i-1][j]+gapStartExt == ga[i][j]){
			gaps++;
			cur = 0;
		}
		i--;
	}
	else{
		seq1Al += s[j-1];
		align += ((inversion)?'x':' '); 
		seq2Al += '-';
		if (ab[i][j-1]+gapStartExt == gb[i][j]){
			gaps++;
			cur = 0;
		}
		j--;
	}
}

inline bool aligner_inv::check_errors(int i, 
								int j, 
								int bstart,
								int cur,
								int ** ab, 
								int ** ga, 
								int ** gb){

	int errors = 0;

	if(bstart > 0)errors++;

	while(i > 0 || j > 0){
		if(cur == 0){

			if(i == 0){
				cur = 2;
				continue;
			}
			else if(j == 0){
				cur = 1;
				continue;
			}

			if((ab[i-1][j-1] + mat) != ab[i][j] && (ab[i-1][j-1] + mis) != ab[i][j]){
				if((ga[i-1][j-1] + mat) == ab[i][j] || (ga[i-1][j-1] + mis) == ab[i][j] ){
					cur = 1;
					if((ga[i-1][j-1] + mis) == ab[i][j])errors++;
				}
				else if((gb[i-1][j-1] + mat) == ab[i][j] || (gb[i-1][j-1] + mis) == ab[i][j]){
					cur = 2;
					if((gb[i-1][j-1] + mis) == ab[i][j])errors++;
				}
			}
			else if((ab[i-1][j-1] + mis) == ab[i][j]){
				errors++;
			}
			i--;	j--;
		}
		else if(cur == 1){
			if ((ab[i-1][j]+gapStartExt) == ga[i][j]){
				errors++;
				cur = 0;
			}
			i--;
		}
		else{
			if ((ab[i][j-1]+gapStartExt) == gb[i][j]){
				errors++;
				cur = 0;
			}
			j--;
		}

		if(errors > max_errors)return true; //improves performance but may mislead user
	}
	return false;
}

bool aligner_inv::print_alignment(input* in, FILE* fo, FILE* fo_full, int* coverage, char ref){

	string s = in->s;
	string t = in->t;
	int i = t.length();
	int j = s.length();
	int L1 = s.length();
	int L2 = t.length();
	int th = in->thID;
	int tLen, sLen, max, maxI, curCur;
	int matches = 0;
	int mismatches = 0;
	int gaps = 0;
	int cur = 0;
	int curI = 0;
	int curMax = MIN_INT;
	int curMaxI = MIN_INT;
	double l, identity;
	bool useInv = false;
	bool invFound = false;
	bool dupFound = false;
	inversion inv;
	duplication dup;
	string seq1Al = "";
	string align = "";
	string seq2Al = "";
	string tSub, sSub, rs, header;
	string org, abb;

	//x set to L1 and L2 for global alignment starting from bottom right corner
	for (int x = L1; x <= L1; x++){
		if(w[N][L2][x] > gs[N][L2][x] && w[N][L2][x] > gt[N][L2][x]){
			max = w[N][L2][x];
			curCur = 0;
		}
		else if(gs[N][L2][x] > gt[N][L2][x]){
			max = gs[N][L2][x];
			curCur = 1;
		}
		else{
			max = gt[N][L2][x];
			curCur = 2;
		}

		if(w[Y][L2][x] > gs[Y][L2][x] && w[Y][L2][x] > gt[Y][L2][x]){
			maxI = w[Y][L2][x];
			curCur = 0;
		}
		else if(gs[Y][L2][x] > gt[Y][L2][x]){
			maxI = gs[Y][L2][x];
			curCur = 1;
		}
		else{
			maxI = gt[Y][L2][x];
			curCur = 2;
		}

		if(max > curMax){
			curMax = max;
			j = x;
			useInv = false;
			cur = curCur;
		}
		
		if(maxI > curMax){
			curMax = maxI;
			j = x;
			useInv = true;
			cur = curCur;
		}
	}

	while(i > 0 || j > 0 ){
		
		opt[i] = j;

		if(useInv){

			if(MODE == MODE_INV_ONLY){
				inv = invs[i][j];
			}
			else if(MODE == MODE_DUP_ONLY){
				dup = dups[i][j];
			}
			else{
				inv = invs[i][j];
				dup = dups[i][j];
			}

			if((MODE == MODE_INV_ONLY || MODE == MODE_BOTH) && ((inv.score == w[Y][i][j] && inv.end == cur) || (inv.score == gs[Y][i][j] && inv.end == cur) || (inv.score == gt[Y][i][j] && inv.end == cur))){
					
					tLen = inv.i-inv.g;
					sLen = inv.j-inv.h;
					tSub = t.substr(inv.g, tLen);
					sSub = s.substr(inv.h, sLen);
					rs = rev_comp(sSub);
					org = /*s[inv.h-1] + */sSub; 
					abb = /*t[inv.g-1] + */tSub;

					//DEBUG
					/*cout << "BestInv: " << inv.g << " " << inv.i << " " << inv.h << " " << inv.j << " " << inv.score << endl;
					cout << rs << endl;
					cout << tSub << endl;*/

					global_align(rs, tSub, wI[inv.start], gsI[inv.start], gtI[inv.start], maxIndel);

					int x = tLen;
					int y = sLen;

					while( x > 0 || y > 0 ){
						traceback(x, y, cur, matches, mismatches, gaps, rs, tSub, seq1Al, seq2Al, align, wI[inv.start], gsI[inv.start], gtI[inv.start], true);
					}

					i = inv.g; j = inv.h;
					useInv = false;
					invFound = true;
					cur = inv.start;
			}
			else if((MODE == MODE_DUP_ONLY || MODE == MODE_BOTH) && ((dup.score == w[Y][i][j] && dup.start == cur) || (dup.score == gs[Y][i][j] && dup.start == cur) || (dup.score == gt[Y][i][j] && dup.start == cur))){
				
					tLen = (dup.i2 - dup.g2);
					i = i - tLen;
					tSub = t.substr(dup.g2, tLen);
					reverse( tSub.begin(), tSub.end() );

					for(int x = 0; x < tLen; x++){
						seq1Al += '-';
						align += (t[dup.i1-x-1] == t[dup.i2-x-1]) ? 'D' : 'd'; 
					}

					seq2Al += tSub;
					org = s[j-1];

					reverse(tSub.begin(), tSub.end());
					abb = s[j-1] + tSub;

					useInv = false;
					dupFound = true;
					mismatches += ((dup.errors-gapStart)/mis);//dup.errors;
					cur = dup.start;
					//gaps++; //to be fair, although probably both should include this
			}
			else{
					traceback(i, j, cur, matches, mismatches, gaps, s, t, seq1Al, seq2Al, align, w[Y], gs[Y], gt[Y], false);
			}
		}
		else{
			traceback(i, j, cur, matches, mismatches, gaps, s, t, seq1Al, seq2Al, align, w[N], gs[N], gt[N], false);
		}
	}

	reverse( seq1Al.begin(), seq1Al.end() );
	reverse( align.begin(), align.end() );
	reverse( seq2Al.begin(), seq2Al.end() );

	//DEBUG print result
	/*cout << seq1Al << endl;
	cout << align << endl;
	cout << seq2Al << endl;*/

	int refLen = s.length();
	int assLen = t.length();
	int ev_gen_start, ev_gen_end, ev_ass_start, ev_ass_end;

	l = (!dupFound) ? 1+(fabs(refLen-assLen)) : 1+(fabs(refLen-(assLen-tLen)));
	identity = 1-(mismatches+gaps+log(l))/assLen;
	identity *= 100;

	if(identity >= min_identity && (invFound || dupFound)){

		if(invFound){ 
			ev_gen_start = (in->loc + inv.h); //one-based 
			ev_gen_end = (((in->loc + inv.h) + in->offset) + sLen) - 1; //inclusive
			ev_ass_start = inv.h;
			ev_ass_end = inv.j;
		}
		else if(dupFound){

			dup.h1 = opt[dup.g1];
			dup.j1 = opt[dup.i1];

			if(dup.j1-dup.h1 != tLen){
				pthread_mutex_unlock( &mutex2 );
				return false; //Quick and dirty fix, think of something better
			}	

			ev_gen_start = (in->loc + dup.j2-1 - in->offset);
			ev_ass_start = dup.g2;
			ev_ass_end = dup.i2;
		}

		int min = 10000, max = -1, sum = 0, avg = 0;

		for (int kk=ev_ass_start; kk<ev_ass_end; kk++)
		{
			if (coverage[kk]>max){
				max = coverage[kk];
			}
			if (coverage[kk]<min){
				min = coverage[kk];
			}
			sum += coverage[kk];
		}

		avg = sum/(ev_ass_end-ev_ass_start);

		if(avg >= min_support){

			//pthread_mutex_lock( &mutex2 );

			if(invFound){
				if(fo != NULL){
					if(in->offset > 0){
						if(ref == '\0')ref = s.at(inv.h);

						fprintf(fo, "%s\t%d\t.\t%c\t<INV>\t%f\tPASS\tSVTYPE=INV;END=%d;Cluster=%d;Contig=%d;MinSupport=%d;AvgSupport=%d;MaxSupport=%d;Identity=%f\n", 
								in->chr.c_str(), ev_gen_start, ref, (-10*log((100-identity)/100)), ev_gen_end, in->cluster, in->contig, min, avg, max, identity); //Should be inv.h-1 but sometimes not enough reference
					}
					else{
						fprintf(fo, "%s\t%d\t.\t%s\t%s\t%f\tPASS\tSVTYPE=INV;END=%d;Cluster=%d;Contig=%d;MinSupport=%d;AvgSupport=%d;MaxSupport=%d;Identity=%f\n", 
								in->chr.c_str(), ev_gen_start, org.c_str(), abb.c_str(), (-10*log((100-identity)/100)), ev_gen_end, in->cluster, in->contig, min, avg, max, identity);
					}
				}
			}
			else if(dupFound){

				if(fo != NULL){
					fprintf(fo, "%s\t%d\t.\t%s\t%s\t%f\tPASS\tSVTYPE=DUP;Cluster=%d;Contig=%d;MinSupport=%d;AvgSupport=%d;MaxSupport=%d;Identity=%f;SOURCE=%d,%d\n", 
							in->chr.c_str(), ev_gen_start, org.c_str(), abb.c_str(), (-10*log((100-identity)/100)), in->cluster, in->contig, min, avg, max, identity, (in->loc + dup.h1 - in->offset), ((in->loc + dup.h1 - in->offset) + tLen)-1); 
				}
			}

			//Full Printing 
			//========================
		 if(invFound){ 
					header = ">\t" + to_string(in->cluster) 
									+ "\t" + to_string(in->contig)
									+ "\t" + to_string(avg)//in->support)
									+ "\tchr" + in->chr 
									+ "\t" + to_string(in->loc + inv.h-1 + 1) //VCF coor
									+ "\t" + to_string(((in->loc + inv.h-1) + in->offset) + sLen) 
									+ "\t" + to_string(identity);

					if(in->offset > 0){
						header = header + "\tClipped";
					}
					else{
						header = header + "\tNormal";
					}

					if(fo_full == NULL){
						cout << header << endl;
						cout << seq1Al << endl;
						cout << align << endl;
						cout << seq2Al << endl;
					}
					else{
						fprintf(fo_full, "%s\n%s\n%s\n%s\n", header.c_str(), seq1Al.c_str(), align.c_str(), seq2Al.c_str());
					}
			}
			else if(dupFound){

					header = ">\t" + to_string(in->cluster)
									+ "\t" + to_string(in->contig) 
									+ "\t" + to_string(avg)//in->support)
									+ "\tchr" + in->chr 
									+ "\t" + to_string(in->loc + dup.h1-1 - in->offset + 1) //VCF coor
									+ "\t" + to_string((in->loc + dup.h1-1 - in->offset) + tLen)
									+ "\t" + to_string(identity)
									+ "\t" + to_string(in->loc + dup.j2-1 - in->offset);

					if(fo_full == NULL){
						cout << header << endl;
						cout << seq1Al << endl;
						cout << align << endl;
						cout << seq2Al << endl;
					}
					else{
						fprintf(fo_full, "%s\n%s\n%s\n%s\n", header.c_str(), seq1Al.c_str(), align.c_str(), seq2Al.c_str());
					}
			}
		//	pthread_mutex_unlock( &mutex2 );
		}
	}
	else{
		return false;
	}

	return (invFound || dupFound);
}

void aligner_inv::calculate_duplications(input* in){

	string s = in->s;
	string t = in->t;
	int L1 = s.length(); //reference
	int L2 = t.length(); //assembly
	int th = in->thID;
	int ming = L2-minLen;
	int maxg = L2-minLen;
	int x = 1, y = 1, g = 1, i = 1, j = 1;
	int curStart = 0;
	int curLen, curDup, bestScore, error, xg;
	int oldGapStart, oldGapExt;
	int bestScores[maxLen+1]; //waste a little space but save on subtraction.

	for(i = 1; i <= L2; i++){
		for(j = 1; j <= L1; j++){
			dups[i][j].score = MIN_INT;
		}
	}

	for(i = minLen; i <= maxLen; i++){
		bestScores[i] = i*mat;
	}

	//Initialize matrix so gaps are never created
	oldGapStart = gapStart;
	oldGapExt = gapExt;
	gapStart = MIN_INT;
	gapExt = MIN_INT;
	gapStartExt = MIN_INT;

	global_align(t, t, w[D], gs[D], gt[D], 999999); 

	gapStart = oldGapStart;
	gapExt = oldGapExt;
	gapStartExt = gapStart + gapExt;

	for(i = minLen; i <= L2; i++){
		maxg = maxM(0, (i - maxLen));

		for(g = (i - minLen); g >= maxg; g--){

			curLen = (i-g);

			for(x = (i + curLen); x <= L2; x++){
				dup_errors[g][x] = ((w[D][i][x] - w[D][g][(x-curLen)])-bestScores[curLen]); 
			}
		}

		for(j = 1; j <= L1; j++){
			for(g = (i - minLen); g >= maxg; g--){  

				curLen = (i-g);

				//Check all g' and i' in self alignment for best duplicated region
				for(x = (i + curLen); x <= L2; x++){   //TODO why is this 2i - g?

					if(dup_errors[g][x] >= adj_max_errors){  

						xg = (x-curLen);
						error = dup_errors[g][x]/2 + gapStart;

						if(w[N][g][j] > gs[N][g][j] && w[N][g][j] > gt[N][g][j]){
							curDup = w[N][g][j] + error;
							curStart = NG;
						}
						else if(gs[N][g][j] > gt[N][g][j]){
							curDup = gs[N][g][j] + error;
							curStart = GS;
						}
						else{
							curDup = gt[N][g][j] + error;
							curStart = GT;
						}

						//DEBUG ================================================
					 /* if(i == 35 && j == 25){// && (x-curLen) == 6 && x == 16){
							cerr << "Inner: " << (x-curLen) << " " << x << " " << (x-(x-curLen)) << " " << curDup << " > " << dups[i][j].score << " (" << w[D][i][x] << " - " << w[D][g][(x-curLen)] << ")/" << bestScore << endl;
						}*/

						if(curDup >= dups[i][j].score){
							dups[i][j].i1 = x;
							dups[i][j].g1 = xg;
							dups[i][j].i2 = i;
							dups[i][j].g2 = g;
							dups[i][j].j2 = j;
							dups[i][j].start = curStart;
							dups[i][j].errors = error;
							dups[i][j].score = curDup;
						}

						if(w[N][xg][j] > gs[N][xg][j] && w[N][xg][j] > gt[N][xg][j]){
							curDup = w[N][xg][j] + error;
							curStart = NG;
						}
						else if(gs[N][xg][j] > gt[N][xg][j]){
							curDup = gs[N][xg][j] + error;
							curStart = GS;
						}
						else{
							curDup = gt[N][xg][j] + error;
							curStart = GT;
						}

						if(curDup >= dups[x][j].score){
							dups[x][j].i1 = i;
							dups[x][j].g1 = g;
							dups[x][j].i2 = x; 
							dups[x][j].g2 = xg;
							dups[x][j].j2 = j;
							dups[x][j].start = curStart;
							dups[x][j].errors = error;
							dups[x][j].score = curDup;
						}
					}
				}//x
			}//g
		}//j
	}//i
}

int aligner_inv::align_inv(input* in){

	if(!verify_input(in)){
		return 1;
	}

	string s = in->s;
	string t = in->t;
	string sSub, tSub;
	string rs = rev_comp(s);
	int L1 = s.length(); //reference
	int L2 = t.length(); //assembly
	int th = in->thID;
	int ming = L2-minLen;
	int tLen, sLen, curLen;
	int minh, maxh, maxi, curInv;
	int bestInv, errors;
	int c, f1, f2, f3;
	int x = 15, y = 15, g = 1, h = 1, i = 1, j = 1, m = 0;
	int bg = 0, bh = 0, bstart = 0, bend = 0, curStart = 0, curEnd = 0; 
	int scores[3];
	int endInv[3];
    bool runDUP = (MODE == MODE_DUP_ONLY || MODE == MODE_BOTH) ? true : false;
    bool runINV = (MODE == MODE_INV_ONLY || MODE == MODE_BOTH) ? true : false;

	if(runINV){
		for(i = 1; i <= L2; i++){
			for(j = 1; j <= L1; j++){
				invs[i][j].score = MIN_INT;
			}
		}
	}

	global_align(s, t, w[N], gs[N], gt[N], 9999); //We could perhaps limit this too

	if(runDUP){
		calculate_duplications(in);
	}

	for(j = 1; j <= L1; j++){

		//Base score of alignment at (i,j) with up to one inversion.
		for(i = 1; i <= L2; i++){

			c = cost(t[i-1],s[j-1],mat,mis);
			gs[Y][i][j] = maxM(gs[Y][i-1][j] + gapExt, w[Y][i-1][j] + gapStartExt);
			gt[Y][i][j] = maxM(gt[Y][i][j-1] + gapExt, w[Y][i][j-1] + gapStartExt);
			w[Y][i][j] = maxM(maxM(w[Y][i-1][j-1], gs[Y][i-1][j-1]), gt[Y][i-1][j-1]) + c;

			//DUPLICATIONS =============================================================
			if(runDUP && dups[i][j].score != MIN_INT){

				//DEBUG -------------------------------------
				/*if(i == 35 && j == 25){
					cerr << t[dups[i][j].g1-1] << t[dups[i][j].g1] << " " << t[dups[i][j].g2-1] << t[dups[i][j].g2] << endl;
					cerr << s[j-1] << s[j] << s[j+1] << " " << t[i-1] << t[i] << t[i+1] << endl;
					cerr << dups[i][j].score << " " << w[N][i-(dups[i][j].i1-dups[i][j].g1)][j] << " " << gs[N][i-(dups[i][j].i1-dups[i][j].g1)][j] << " " << gt[N][i-(dups[i][j].i1-dups[i][j].g1)][j] << endl;
					cerr << dups[i][j].score << " " << w[Y][i][j] << " " << gs[Y][i][j] << " " << gt[Y][i][j] << endl;
				}*/

				if(dups[i][j].score > maxM(w[Y][i][j],maxM(gs[Y][i][j],gt[Y][i][j]))){
					if(dups[i][j].start == NG){
						w[Y][i][j] = dups[i][j].score;
					}
					else if(dups[i][j].start == GS){
						gs[Y][i][j] = dups[i][j].score;
					}
					else{
						gt[Y][i][j] = dups[i][j].score;
					}
				}
				else{
					dups[i][j].score = MIN_INT;
				} 
			}
		}

		for(g = ming; g >= 0; g--){ //find a way to skip redundant iterations

			if(runINV){
				tLen = minM(maxLen, L2-g);
				sLen = minM(maxLen, j);

				if((tLen-sLen) >= maxIndel){
					tLen = sLen + maxIndel;
				}
				else if((sLen-tLen) >= maxIndel){
					sLen = tLen + maxIndel;
				}

				tSub = t.substr(g, tLen);
				sSub = rs.substr(L1-j, sLen);


				//Align the largest possible sequences, given j and g, for all three cases: Match, Gap on S, Gap on T
				global_align(sSub, tSub, wI[NG], gsI[NG], gtI[NG], maxIndel);
				global_align(sSub, tSub, wI[GS], gsI[GS], gtI[GS], maxIndel);
				global_align(sSub, tSub, wI[GT], gsI[GT], gtI[GT], maxIndel);
			}

			//INVERSIONS =================================================
			//Check if inversion beats current alignment at (i,j) by adding inversion score to alignment score without inversions at (g,h).
			if(runINV){
				maxi = minM(L2, (g+maxLen));
				for(i = g+minLen; i <= maxi; i++){ 

					bestInv = MIN_INT;
					inversion inv;
					curInv = bestInv;

					x = (i-g);
					maxh = minM(j-minLen, j-(x-maxIndel));
					minh = maxM(maxM(0, (j-maxLen)), j-(x+maxIndel));

					for(h = maxh; h >= minh; h--){
						
						y = (j-h);

						for(m = 0; m < 3; m++){
							if(wI[m][x][y] > gsI[m][x][y] && wI[m][x][y] > gtI[m][x][y]){
								scores[m] = wI[m][x][y];
								endInv[m] = NG;
							}
							else if(gsI[m][x][y] > gtI[m][x][y]){
								scores[m] = gsI[m][x][y];
								endInv[m] = GS;
							}
							else{
								scores[m] = gtI[m][x][y];
								endInv[m] = GT;
							}
						}

						f1 = w[N][g][h] + scores[NG];
						f2 = gs[N][g][h] + scores[GS];
						f3 = gt[N][g][h] + scores[GT];

						if(f1 >= f2 && f1 >= f3){
							curInv = f1;
							curStart = NG;
							curEnd = endInv[NG];
						}
						else if(f2 >= f3){
							curInv = f2;
							curStart = GS;
							curEnd = endInv[GS];
						}
						else{
							curInv = f3;
							curStart = GT;
							curEnd = endInv[GT];
						}

						if(curInv >= bestInv){
							bestInv = curInv;
							bg = g;
							bh = h;
							bstart = curStart;
							bend = curEnd;
						}

						//DEBUG ------------------------------------------------
						//cout << g << " " << i << " " << h << " " << j << endl;
					 /* if(g == 94 && i == 228 && h == 94 && j == 228){

							print_matrix(wI[GS], sSub, tSub);

							cout << "curInv " << curInv << " " << g << " x " << x << " y " << y << endl;
							cout << "bestInv " << bestInv << " " << bg << endl;
							for(m = 0; m < 3; m++){
								cout << scores[m] << endl;
							}
							cout << "-----" << endl;
							for(m = 0; m < 3; m++){
								cout << endInv[m] << endl;
							}
							cout << "-----" << endl;
							cout << w[N][g][h] << endl;
							cout << gs[N][g][h] << endl;
							cout << gt[N][g][h] << endl;
							cout << tSub << " " << tSub[x-1] << endl;
							cout << sSub << " " << sSub[y-1] << endl;
							cout << "curEnd " << curEnd << endl;
						}*/

						inv.score = (bestInv + invPen);

						if(inv.score > invs[i][j].score && inv.score > dups[i][j].score){

							if(check_errors((i-bg), (j-bh), bstart, bend, wI[bstart], gsI[bstart], gtI[bstart]))continue;

							inv.i = i;
							inv.j = j;
							inv.g = bg;
							inv.h = bh;
							inv.start = bstart;
							inv.end = bend;

							if(bend == NG && inv.score > w[Y][i][j]){
								w[Y][i][j] = inv.score;
								invs[i][j] = inv;
							}
							else if(bend == GS && inv.score > gs[Y][i][j]){
								gs[Y][i][j] = inv.score;
								invs[i][j] = inv;
							}
							else if(bend == GT && inv.score > gt[Y][i][j]){
								gt[Y][i][j] = inv.score;
								invs[i][j] = inv;
							}
						}
					}//h
				}//i
			}
		}
	}

	return 0;
//cout << "Iterations: " << iter << endl;
//DEBUG -------------------------
/*	print_matrix(w[N], s, t);
	cout << endl;
	print_matrix(w[Y], s, t);
	cout << endl;*/
}

bool aligner_inv::verify_input(input* in){

	if(in->s.empty() || in->t.empty() || in->chr.empty()){
		cerr << "Error: One of the input sequences is empty:" << endl;
		return false;
	}

	for(int i = 0; i < in->s.length(); i++){
		if((find(begin(VALID_CHARS), end(VALID_CHARS), in->s[i]) == end(VALID_CHARS))){
		//if(in->s[i] != 'A' && in->s[i] != 'T' && in->s[i] != 'C' && in->s[i] != 'G' && in->s[i] != 'N'){
			cerr << "Error: Invalid character \"" << in->s[i] << "\" in reference:" << endl;
			return false;
		}
	}

	for(int i = 0; i < in->t.length(); i++){
		if((find(begin(VALID_CHARS), end(VALID_CHARS), in->t[i]) == end(VALID_CHARS))){
		//if(in->t[i] != 'A' && in->t[i] != 'T' && in->t[i] != 'C' && in->t[i] != 'G' && in->t[i] != 'N'){
			cerr << "Error: Invalid character \"" << in->t[i] << "\" in contig:" << endl;
			return false;
		}
	}

	if(in->thID < 0){
		cerr << "Error: Invalid thread ID: " << in->thID << endl;
		return false;
	}

	if(in->loc < 0){
		cerr << "Error: Invalid reference location: " << in->loc << endl;
		return false;
	}

	if(in->offset < 0){
		cerr << "Error: Invalid offset: " << in->offset << endl;
		return false;
	}

	return true;
}

int aligner_inv::readLines(input* in){

	string s, t, info;
	char buf[MAX_CHARS_PER_LINE];
	int ret_val=0;

	pthread_mutex_lock( &mutex1 );
	//if(err != 0)cout << err << endl;

	if(!fin.eof()){
		fin.getline(buf, MAX_CHARS_PER_LINE);
		if(fin.good()){
			if(buf[0] == '>'){
				info = buf;
				fin.getline(buf, MAX_CHARS_PER_LINE);
				s = buf;
				in->s = s;
				fin.getline(buf, MAX_CHARS_PER_LINE);
				t = buf;
				in->t = t;

				//TODO parse info line

				if(s.length() > maxSeqLen || t.length() > maxSeqLen){
					size_t posClus = info.find_first_of('M');
					cerr << "Sequence too long (" << (maxM(s.length(), t.length())) << "nt), skipping cluster " << info.substr(10, posClus-12) << endl;
					ret_val = 2;
				}
			}
		}
		else{
			ret_val = 1;
		}
	}
	else{
		ret_val = 1;
	}
	pthread_mutex_unlock( &mutex1 );


	return ret_val;
}

void* aligner_inv::run(){

		int ret_val = 0;
		int orgMaxIndel = maxIndel;
		uint64 timeBefore = 0;
		uint64 timeAfter = 0;

		input in;
		in.thID = curThID;
		curThID++;

		while(true){
			
			ret_val = readLines(&in);

			if(ret_val == 2){
					continue;
			}
			else if(ret_val == 1){
					exit(0);
			}

			if(dynamic){
				maxIndel = minM(abs(int(in.t.length()-in.s.length())), orgMaxIndel);
			}

			align_inv(&in);

			print_alignment(&in, NULL, NULL, NULL, '\0');

		}
		return NULL;
}

int aligner_inv::run_file(string filename){

	fin.open(filename);
	if (!fin.good()) 
		return 1; 

	for (int i = 0; i < num_threads; i++)
		pthread_create(threads + i, NULL, &aligner_inv::run_helper, this);

	for (int i = 0; i < num_threads; i++)
		pthread_join(threads[i], NULL);

	fin.close();

	return 0;
}

void aligner_inv::setDynamic(bool dyn){
	dynamic = dyn;
}

void aligner_inv::setMinIndentity(double iden){
	min_identity = iden;
}

void aligner_inv::setMaxErrors(int errors){
	max_errors = errors;
	adj_max_errors = (max_errors*2)*mis;
}

void aligner_inv::setMinLen(int min){
	minLen = min;
}

void aligner_inv::setMaxLen(int max){
	maxLen = max;
}

void aligner_inv::setMaxIndel(int indel){
	maxIndel = indel;
}

void aligner_inv::setInvPenalty(int inv){
	invPen = inv;
}

void aligner_inv::setGapStart(int start){
	gapStart = start;
	gapStartExt = gapStart + gapExt;
}

void aligner_inv::setGapExtend(int ext){
	gapExt = ext;
	gapStartExt = gapStart + gapExt;
}

void aligner_inv::setMismatchPenalty(int miss){
	mis = miss;
}

void aligner_inv::setMatchScore(int match){
	mat = match;
}

void aligner_inv::setMode(int mode){
	MODE = mode;
}
