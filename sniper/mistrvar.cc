#include <iostream>
#include <fstream>
#include <string>
#include <unordered_map>
#include <algorithm>
#include <limits>
#include <cctype>
#include <math.h>
#include <pthread.h>
#include <unistd.h>
#include <sstream>
#include <sys/time.h>
#include "mistrvar.h"


using namespace std;

mistrvar::mistrvar(const string &partition_file, const string &reference) : 
	variant_caller(partition_file, reference){

}

mistrvar::~mistrvar(){


}

void mistrvar::init(){


}

void mistrvar::run_mistrvar( const string &range, const string &out_vcf, const string &out_full, int global_mode, int max_len, int max_error,
	 int min_support, int combined_identity, int single_identity, double final_identity, int ref_flank, const int LEGACY_ASSEMBLER)
{
	
	aligner al_fwd(ANCHOR_SIZE, (max_len+ref_flank*2+CLIPPING_BUFFER+MAX_ASSEMBLY_RANGE+1)); //+1 to avoid off-by-one error, too lazy to check if it actually happens
	aligner al_rev(ANCHOR_SIZE, (max_len+ref_flank*2+CLIPPING_BUFFER+MAX_ASSEMBLY_RANGE+1));
	aligner_inv al_inv(max_len);
	aligner_inv::input in;
	in.thID = 0;

	al_inv.setMinIndentity(final_identity); 
	al_inv.setMaxErrors(max_error);
	al_inv.setMaxIndel(MAX_INDEL);

	string con, type;
	int coverage[(max_len + CLIPPING_BUFFER)];
	int mode = aligner_inv::MODE_BOTH;
	double fwdIden = 0;
	double revIden = 0;
	double maxInvLen, minInvLen, maxDupLen;
	int cutS, cutE, fwdS, fwdE, fwdAS, fwdAE, revS, revE, revAS, revAE, start, end, correction, NCount, NCount_int, SV_predicted_len, contig_id;
	bool run_align = false;
	char first_char = '\0';

	FILE *fo_vcf = fopen(out_vcf.c_str(), "wb");
	FILE *fr_vcf = fopen((out_vcf + ".reads").c_str(), "wb");
	FILE *fo_full = fopen(out_full.c_str(), "wb");
	FILE *fr_full = fopen((out_full + ".reads").c_str(), "wb");

	while (1) {
		auto p = pt.read_partition(part_file, range, min_support, MAX_READS_PER_PART);

		if (!p.size()) 
			break;

		// fprintf(stderr,"EHA %d\n", p.size());
		// for (auto &x: p) {
		//	fprintf(stderr,"%s %s %d\n", x.first.first.c_str(), x.first.second.c_str(), x.second); 
		// }

		//fprintf(stderr, "start to ass\n");
		
		vector<contig> contigs;
		if(LEGACY_ASSEMBLER){
			contigs = as_old.assemble(p); //fprintf(stderr, "Greedy Assembler\n");
		}
		else{
			vector<string> reads;

			for (int i = 0; i < p.size(); i++) {
				reads.push_back(p[i].first.second);
			}
			contigs = as.assemble(reads); //fprintf(stderr, "OLC Assembler\n");

			reads.clear();
		}

		
		contig_id = 0;
		//fprintf(stderr, "end ass\n");
		
		for (auto &contig: contigs){ 
			if (contig.support() >= min_support) {

				int ref_len = ref.get_ref_length(pt.get_reference());
				int ref_start = pt.get_start() - ref_flank;
				int ref_end = pt.get_end() + ref_flank;

				if(ref_start < 1){
					ref_start = 1;
				}

				if(ref_end > ref_len){
					ref_end = ref_len;
				}

			//	fprintf(stderr, "from %d to %d, %s\n", ref_start, ref_end, pt.get_reference().c_str());
				string gen = ref.extract(pt.get_reference(), ref_start, ref_end);
				//fprintf(stderr, "done\n");

				if (contig.data.size() > 10000 || gen.size() > (10000+ref_flank*2)) {
					fprintf(stderr, "WEIRD CASE Too long region -- Contig %d Genomic %d!\n", contig.data.size(), gen.size());
					for (auto &e: contig.read_information)
						fprintf(stderr, "%d %d %s %s\n", e.location_in_contig, e.location, e.seq.c_str(), e.name.c_str());
					continue;
				}

				if(contig.data.length() > max_len + CLIPPING_BUFFER){ //Even clipping won't fix 2 x read length
					continue;
				}

				if(contig.support() > 1){
					cutS = contig.read_information[1].location_in_contig;
					cutE = contig.read_information[contig.support()-2].location_in_contig+contig.read_information[contig.support()-2].seq.length();
					con = contig.data.substr(cutS, cutE-cutS);
				}
				else{ //one read
					con = contig.data;
				}

				int con_len = con.length();
				type = "Error";

				//if(ref_start <	DEBUG && ref_end > DEBUG){
					cerr << contig.support() << endl;
					cerr << coverage[0] << " " << coverage[con_len-1] << endl;
					cerr << con[0] << " == " << contig.read_information[1].seq[0] << endl;
					cerr << con[con_len-1] << " == " << contig.read_information[contig.support()-1].seq[contig.read_information[contig.support()-1].seq.length()-1] << endl;
					cerr << gen << endl;
					cerr << con << endl;
					cerr << con_len << endl;
					cerr << contig.support() << endl;
					//cerr << ref.extract(pt.get_reference(), 368462, 368492) << endl;
				//}

				if(con_len > max_len){
					continue;
				}

				double AT_count = 0, CG_count = 0;

				for(int i = 0; i < con_len-1; i++){
					if((con[i] == 'A' && con[i+1] == 'T') ||(con[i] == 'T' && con[i+1] == 'A')){
						AT_count++;
					}
					if((con[i] == 'C' && con[i+1] == 'G') ||(con[i] == 'G' && con[i+1] == 'C')){
						CG_count++;
					}
					//TODO add polynucleotide checking
				}

				if(AT_count/(double)con_len >= MAX_AT_CG || CG_count/(double)con_len >= MAX_AT_CG)continue;

				al_fwd.align(gen, con);
				fwdIden = al_fwd.get_identity();
				fwdS = al_fwd.get_start() + ref_start; 
				fwdE = al_fwd.get_end() + ref_start; 
				fwdAS = al_fwd.get_anchor_start()+1;
				fwdAE = al_fwd.get_anchor_end()+1;
				SV_predicted_len = al_fwd.get_SV_predicted_len();// + OVERLAP;
				int unaligned = max(fwdAS, (con_len-fwdAE));

				/*if(ref_start <	DEBUG && ref_end > DEBUG){
					al_fwd.dump(fo_full);
					cerr << "fwdS " << fwdS << " fwdE " << fwdE << " fwdAS " << fwdAS << " fwdAE " << fwdAE << " iden " << fwdIden << endl;
				}*/

				//CASE 1: Forward alignment is good without room for inversion up/downstream, use paramters for short inversion
				if(fwdIden >= single_identity && unaligned <= MIN_INV_LEN_DEFAULT){//ANCHOR_SIZE){
	
					maxInvLen = SV_predicted_len;
					maxDupLen = min(50, max((fwdAE-fwdAS+1), con_len)-(fwdE-fwdS+1));
					maxInvLen = max(maxInvLen, maxDupLen);
					minInvLen = MIN_INV_LEN_DEFAULT;	

					type = "Fwd";

					if(maxInvLen >= MIN_INV_LEN_DEFAULT){

						in.s = ref.extract(pt.get_reference(), (fwdS-fwdAS+1), fwdE+(con_len-fwdAE));

						NCount = 0;
						NCount_int = 0;
						for(int i = 0; i < in.s.length(); i++){
							if(in.s[i] == 'N'){
								NCount++;
								if(i > fwdAS && i < fwdAE){
									NCount_int++;
								}
							}
						}

						if(NCount_int >= MIN_INV_LEN_DEFAULT){
							continue;
						}

						if(NCount >= MIN_INV_LEN_DEFAULT){
							in.s = ref.extract(pt.get_reference(), fwdS, fwdE);
							start = fwdS;
							end = fwdE; 
						}
						else{
							start = fwdS-fwdAS+1;
							end = fwdE+(con_len-fwdAE);
							if(start < 1)start = 1;
						}

						in.offset = 0;

						if(maxDupLen < MIN_INV_LEN_DEFAULT){
							mode = aligner_inv::MODE_INV_ONLY;
		//cout << "INV " ;
						}
						else{
							mode = aligner_inv::MODE_BOTH;
		//cout << "BOTH " ;
						}
					}
					else{
						mode = aligner_inv::MODE_NONE;
					}
					//===============================================
				}
				else if(fwdIden > 0){ //No anchor

					if(fwdAS < ANCHOR_SIZE && (con_len-fwdAE) < ANCHOR_SIZE){
						gen = ref.extract(pt.get_reference(), (fwdS-fwdAS+1), (fwdE+(con_len-fwdAE)));
						revS = fwdS-fwdAS+1; 
						revE = fwdS-fwdAS+1;
					}
					else if(fwdAS < ANCHOR_SIZE){
						gen = ref.extract(pt.get_reference(), (fwdS-fwdAS+1), ref_end);
						revS = fwdS-fwdAS+1; 
						revE = fwdS-fwdAS+1;
					}
					else if((con_len-fwdAE) < ANCHOR_SIZE){
						gen = ref.extract(pt.get_reference(), ref_start, (fwdE+(con_len-fwdAE)));
						revS = ref_start; 
						revE = ref_start;
					}
					else{
						revS = ref_start; 
						revE = ref_start; 
					}

					al_rev.align(gen, reverse_complement(con));
					revIden = al_rev.get_identity();
					revS += al_rev.get_start();
					revE += al_rev.get_end();
					revAS = con_len-al_rev.get_anchor_end();
					revAE = con_len-al_rev.get_anchor_start();
					unaligned = max(revAS, (con_len-revAE));
					//al.dump(fo_full);

					if((double)((fwdAE-fwdAS)+(revAE-revAS))/(double)con_len < .7)continue;

					//CASE 2: Reverse alignment is good without room for forward aligned anchor, use parameters for long inversion
					if(revIden >= single_identity && unaligned <= ANCHOR_SIZE){
						//Remove since it generates too many FP
						continue;
					}
					else if(fwdIden + revIden >= combined_identity){ //CASE 3: Combined identities are good, use parameters for medium inversion
						start = min(fwdS,revS);
						end = max(fwdE,revE);
						maxInvLen = SV_predicted_len;
						minInvLen = ((revIden-((fwdIden+revIden)-100)/2)/100)*(double)(revE-revS+1); //Inversion length lower bound is the reverse identity - half of calculated overlap * aligned reverse contig
						type = "Combined";

						/*cerr << start << " " << end << endl;
						cerr << fwdS << " -f- " << fwdE << " " << revS << " -r- " << revE << endl;
						cerr << fwdAS << " -af- " << fwdAE << " " << revAS << " -ar- " << revAE << endl;
						cerr << con << endl;*/

						if(fwdE < revS && fwdAS < revAS && fwdAE < revAE){//One breakpoint case downstream
							correction = fwdE - (fwdAE-(revAS-1));
							if(correction >= revS)correction = revS-1;
							if(correction-fwdS+1 + revE-revS+1 < con_len)revE += con_len - (correction-fwdS+1 + revE-revS+1); //Careful!
							in.s = ref.extract(pt.get_reference(), fwdS, correction) + ref.extract(pt.get_reference(), revS, revE);
							in.offset = revS - correction - 1;
							first_char = ref.extract(pt.get_reference(), correction+1, correction+1).at(0);
							maxInvLen = SV_predicted_len + correction;
							type = type + " Clipped FR";
							mode = aligner_inv::MODE_INV_ONLY;
	//cout << "INVFR " ;
						}
						else if(fwdS > revE && fwdAS > revAS && fwdAE > revAE){//One breakpoint case upstream
							correction = fwdS + (revAE-(fwdAS-1));
							if(correction <= revE)correction = revE+1; 
							if(revE-revS+1 + fwdE-correction+1 < con_len)revS -= con_len - (revE-revS+1 + fwdE-correction+1);
							in.s = ref.extract(pt.get_reference(), revS, revE) + ref.extract(pt.get_reference(), correction, fwdE);
							in.offset = correction - revE - 1;
							maxInvLen = SV_predicted_len + correction;
							type = type + " Clipped RF";
							mode = aligner_inv::MODE_INV_ONLY;
	//cout << "INVRF " ;
						}
						else{
							in.s = ref.extract(pt.get_reference(), start, end); //All other cases (probably false positives)
							in.offset = 0;
							type = type + " Other";
							//mode = aligner_inv::MODE_BOTH;
							mode = aligner_inv::MODE_INV_ONLY;
	//cout << "INVOTHER " ;
						}
					}
					else if(fwdIden >= single_identity){ 

						if(revIden < 0 && (max((fwdAE-fwdAS+1), con_len) - (fwdE-fwdS+1)) >= MIN_INV_LEN_DEFAULT){
							/*cerr << start << " " << end << endl;
							cerr << fwdS << " -f- " << fwdE << endl;
							cerr << fwdAS << " -af- " << fwdAE	<< endl;
							cerr << con << endl;*/
	//cout << "DUP " ;
							in.s = ref.extract(pt.get_reference(), (fwdS-fwdAS+1), fwdE+(con_len-fwdAE));

							NCount = 0;
							for(int i = 0; i < in.s.length(); i++){
								if(in.s[i] == 'N'){
									NCount++;
								}
							}

							if(NCount >= MIN_INV_LEN_DEFAULT){
								in.s = ref.extract(pt.get_reference(), fwdS, fwdE);
								start = fwdS; 
							}
							else{
								start = fwdS-fwdAS+1;
								if(start < 1)start = 1;
							}

							in.offset = 0;
							maxInvLen = min(50, max((fwdAE-fwdAS+1), con_len)-(fwdE-fwdS)); //length of gap
							minInvLen = MIN_INV_LEN_DEFAULT;
							type = "Duplication";
							mode = aligner_inv::MODE_DUP_ONLY;
						}
						else{
							mode = aligner_inv::MODE_NONE;
						}
					}
					else{
						continue;
					}
				}
				else{
					continue;
				}

				if(in.s.length() <= max_len){

					//Common input parameters
					in.t = con;
					in.chr = pt.get_reference().c_str();
					in.cluster = pt.get_cluster_id();
					in.contig = ++contig_id;
					in.support = contig.support();
					in.loc = start;
					bool found = false;

					if(mode != aligner_inv::MODE_NONE){
						if(global_mode != aligner_inv::MODE_BOTH){
							if(global_mode != mode && mode != aligner_inv::MODE_BOTH){
								continue;
							}
							else{
								mode = global_mode;
							}
						}

						//Run inversion aligner 
						al_inv.setMaxLen(maxInvLen);
						al_inv.setMinLen(minInvLen);
						al_inv.setMode(mode);
						int ret_val = al_inv.align_inv(&in);

						//if(ref_start <	DEBUG && ref_end > DEBUG){
						if(ret_val != 0){ 
							fprintf(stderr, "ERROR DUMP:\n");
							fprintf(stderr, "Cluster_ID: %d\n", in.cluster); //THIS ONE IS CAUSING SEG FAULT
							fprintf(stderr, "Contig_ID: %d\n", in.contig); //THIS ONE IS CAUSING SEG FAULT
							fprintf(stderr, "Ref: %s\n", in.s.c_str());
							fprintf(stderr, "Contig: %s\n", in.t.c_str());
							fprintf(stderr, "Location: %s %d\n", in.chr.c_str(), in.loc);
							fprintf(stderr, "MinLength: %f	MaxLength: %f Mode: %d Type: %s\n", minInvLen, maxInvLen, mode, type.c_str());
							fprintf(stderr, ">Cluster: %d Contig: %d	MaxSupport: %d Reads: \n", in.cluster, in.contig, contig.support());
							for (auto &read: contig.read_information)
								fprintf(stderr, "+ %d %d %s\n", read.location_in_contig, read.seq.size(), read.seq.c_str());
							fprintf(stderr, "Last alignment:");
							al_fwd.dump(stderr);
							al_rev.dump(stderr);
							continue;
						}

						for (int k=0; k < contig.data.length(); k++){
							coverage[k]=0; 
						}

						if(contig.support() > 1){
							for (int j = 0; j < contig.support(); j++){
								for (int k=0; k < contig.read_information[j].seq.length(); k++){
									if(k+contig.read_information[j].location_in_contig-cutS >= 0){		//The coverage beyond the new contig length will be > 0, but it doesn't matter since it will never be accessed. Just FYI
										coverage[k+contig.read_information[j].location_in_contig-cutS]++;
									}
								}
							}
						}

						found = al_inv.print_alignment(&in, fo_vcf, fo_full, coverage, first_char);

					}

					if(found){
// cout << "chr" << in.chr << ": " <<  in.loc << " " << in.cluster << " " << in.contig << in.support << endl;
// cout << in.s << endl;
// cout << in.t << endl;
						fprintf(fr_vcf, ">Cluster: %d Contig: %d MaxSupport: %d Reads: \n", in.cluster, in.contig, contig.support());
						for (auto &read: contig.read_information)
							fprintf(fr_vcf, "+ %d %d %s %s\n", read.location_in_contig, read.seq.size(), read.name.c_str(), read.seq.c_str());
					}
					else{
						//Non-inversion calls 
						//===============================================
						if(fwdIden >= single_identity && type == "Fwd"){
							al_fwd.extract_calls(pt.get_reference(), fwdS, in.cluster, in.contig, final_identity, min_support, coverage, fo_vcf); //use same min as inversion length
						}
					}
				} // if contig size 
	//cout << endl;
			} //if support
		} //while contigs
	} //infinite

	fclose(fo_vcf);
	fclose(fr_vcf);
	fclose(fo_full);
	fclose(fr_full);
}


