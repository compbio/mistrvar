#ifndef __MISTRVAR__
#define __MISTRVAR__

#include <string>
#include "variant_caller.h"
#include "partition.h"
#include "common.h"
#include "assembler.h"
#include "assembler_old.h"
#include "assembler_ext.h"
#include "genome.h"
#include "aligner.h"
#include "aligner_inv.h"

using namespace std;

class mistrvar : public variant_caller
{
private:

	const int MAX_READS_PER_PART = 10000;
	const int ANCHOR_SIZE = 22;
	const double MAX_AT_CG = 0.7;
	const int MIN_INV_LEN_DEFAULT = 5;
	const int MAX_INDEL = 0;
	const int HALF_IDEN = 50;
	const int CLIPPING_BUFFER = 200;
	const int DEBUG = 16034742;

public:

	

private:

	void  init();

public:
	
	mistrvar(const string &partition_file, const string &reference); 
	~mistrvar();

	void run_mistrvar( const string &range, const string &out_vcf, const string &out_full, int global_mode, int max_len, int max_error,
		int min_support, int combined_identity, int single_identity, double final_identity, int ref_flank, const int LEGACY_ASSEMBLER);

};
#endif
