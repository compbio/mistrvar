#ifndef __ALIGNER_
#define __ALIGNER__

#include<string>

using namespace std;

class aligner
{
private:
	static const int MATCH_SCORE = 10;
	static const int MISMATCH_SCORE = -10;
	static const int GAP_OPENING_SCORE = -15;
	static const int GAP_EXTENSION_SCORE = -1;
	static const int GAP_OPEN_AND_EXT_SCORE = GAP_OPENING_SCORE + GAP_EXTENSION_SCORE;
	string a;
	string b;
	string c;
	int **score;
	int **gapa;
	int **gapb;
	double identity;
	double identity_full;
	int p_start;
	int p_end;
	int a_start;
	int a_end;
	int anchor_len_min;
	int max_side;
	int SV_predicted_len;
	int len;
	int p_start_full;
	int p_end_full;
	int len_full;

private:
	void clear(int, int);
	void print_matrix(string, const string &, const string &, int **);	

public:
	aligner(int anchor_length = 1, int max_length = 10000);
	~aligner();
	void align(const string &, const string &);
	void extract_calls(const string&, int, int, int, int, int, int*, FILE*);
	void dump(FILE *fo);
	int get_start();
	int get_end();
	int get_anchor_start();
	int get_anchor_end();
	float get_identity();
	int get_SV_predicted_len();
	void setAnchorLen(int);
};
#endif

