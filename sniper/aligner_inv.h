#ifndef __ALIGNER_INV_
#define __ALIGNER_INV__

#include<string>

using namespace std;

class aligner_inv
{
private:
	typedef unsigned long long uint64;

	const int MAX_CHARS_PER_LINE = 4096;
	const int MIN_INT = -99999;
	const int N = 0;
	const int Y = 1;
	const int D = 2;
	const int NG = 0;
	const int GS = 1;
	const int GT = 2;
	const char VALID_CHARS[16] = {'A','T','U','C','G','M','K','R','Y','B','D','H','V','W','S','N'};

	bool dynamic = false;
	double min_identity = 99;
	int min_support = 2;
	int max_errors = 1;
	int minLen = 5;
	int maxLen = 35;
	int maxIndel = 2;
	int invPen = -33;//-2;
	int gapStart = -10;//-20;
	int gapExt = -1;//-8;
	int mis = -10;//-11; 
	int mat = 10;
	int maxSeqLen = 2000;
	int num_threads = 1;
	int MODE = MODE_INV_ONLY;
	int gapStartExt = gapStart + gapExt;
	int adj_max_errors = (max_errors*2)*mis;

	pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
	pthread_mutex_t mutex2 = PTHREAD_MUTEX_INITIALIZER;

	struct inversion{
	  int g;
	  int h;
	  int i;
	  int j;
	  int score;
	  int start;
	  int end;
	};

	struct duplication{
	  int g1;
	  int i1;
	  int g2;
	  int i2;
	  int h1;
	  int j1;
	  int j2;
	  int score;
	  int errors;
	  int start;
	};

	int*** w;
	int*** gs;
	int*** gt;
	int*** wI;
	int*** gsI;
	int*** gtI;
	inversion** invs;
	duplication** dups;
	int** dup_errors;
	int* opt;

	pthread_t* threads;
  	int curThID;
	ifstream fin;

public:

	static const int MODE_INV_ONLY = 0;
	static const int MODE_DUP_ONLY = 1;
	static const int MODE_BOTH = 2;
	static const int MODE_NONE = 3;

	struct input{
	  string s;
	  string t;
	  string chr;
	  int thID;
	  int cluster;
	  int contig;
	  int support;
	  int loc;
	  int offset;
	};

private:
	void  init(int, int, bool);
	int max3(int, int, int);
	int max4(int, int, int, int);
	void  print_matrix(int **, string, string);
	void  print_matrix(bool **, string, string);
	void  print_matrix(char **, string, string);
	bool verify_input(input*);
	int readLines(input*);
	void global_align(string, string, int **, int **, int **, int);
	void calculate_duplications(input*);

	void traceback(int&, 
                int&, 
                int&,
                int&, 
                int&, 
                int&, 
                string&, 
                string&, 
                string&, 
                string&, 
                string&,
                int **, 
                int **, 
                int **,
                bool);

	bool check_errors(int, 
                int, 
                int,
                int,
                int **, 
                int **, 
                int **);

public:
	aligner_inv(int M = 2000, 
                int p = 1,  
                double m = 99, 
                int s = 2,
                int e = 1, 
                int l = 5, 
                int L = 35, 
                int I = 2, 
                int i = -33, 
                int gs = -10, 
                int ge = -1, 
                int miss = -10, 
                int match = 10,
                int mode = MODE_BOTH, 
                bool dyn = false);
	~aligner_inv();

	string rev_comp(string);

	//Alignment	
	int align_inv(input*);
	bool print_alignment(input*, FILE*, FILE*, int*, char);

	//Run with threading
	void* run();//void*);
	static void *run_helper(void* aligner){
		return ((aligner_inv *)aligner)->run();
	}
	int run_file(string);

	//Setters
	void setDynamic(bool);
	void setMinIndentity(double);
	void setMaxErrors(int);
	void setMinLen(int);
	void setMaxLen(int);
	void setMaxIndel(int);
	void setInvPenalty(int);
	void setGapStart(int);
	void setGapExtend(int);
	void setMismatchPenalty(int);
	void setMatchScore(int);
	void setMode(int);

};
#endif










