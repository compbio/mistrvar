#include<iostream>
#include<string>
#include<algorithm>
#include<cmath>
#include"aligner.h"

#define cost(A,B,C,D) (A==B) ? C : D
#define maxM(A,B) (((A) > (B)) ? (A) : (B))
#define minM(A,B) (((A) < (B)) ? (A) : (B))

using namespace std;
aligner::aligner(int anchor_length, int max_length)
{
	anchor_len_min = anchor_length;
	max_side = max_length;

	score = new int*[max_side+1];
	gapa  = new int*[max_side+1];
	gapb  = new int*[max_side+1];

	for (int i=0; i<= max_side; i++)
	{
		score[i] = new int[max_side+1];
		gapa[i] = new int[max_side+1];
		gapb[i] = new int[max_side+1];
	}

	for (int i=0; i<=max_side; i++){
		score[i][0] = 0;
		gapa[i][0] = -100000;
		gapb[i][0] = -100000;
	}

	for (int j=0; j<=max_side; j++){
		score[0][j] = 0;
		gapa[0][j] = -100000;
		gapb[0][j] = -100000;
	}
}

aligner::~aligner()
{
	for (int i=0; i<= max_side; i++)
	{
		delete score[i];
		delete gapa[i];
		delete gapb[i];
	}
	
	delete score;
}

void aligner::print_matrix(string name, const string &a, const string &b, int **matrix)
{
	fprintf(stdout, "=================================\n");
	fprintf(stdout, "Matrix: %s\n", name.c_str());
	fprintf(stdout, "=================================\n");
	fprintf(stdout,"       -");
	for (int i=0; i<a.length();i++)
		fprintf(stdout, "%4c", a[i]);
	fprintf(stdout, "\n");
	for (int j=0; j<=b.length(); j++)
	{
		if (j>0) fprintf(stdout,"%4c", b[j-1]);
		else fprintf(stdout, "   -");
		for (int i=0; i<=a.length();i++)
		{
			if (matrix[i][j] <= -100000)
				fprintf(stdout, "   N");
			else 
				fprintf(stdout,"%4d", matrix[i][j]);
		}
		fprintf(stdout, "\n");
	}
	fprintf(stdout, "=================================\n");
}

void aligner::clear(int a, int b)
{
	identity = 0;

	for (int i=0; i<a; i++)
		for (int j=0; j<b; j++)
			score[i][j]=0;
	
}

void aligner::align(const string &ref, const string &ass)
{
	len = ass.length();
	len_full = ass.length();

	//clear(ref.length()+1, len+1); //waste of time really. 
	
	int tmp, ii, jj;		
	for (int i=1; i<=ref.length();i++)
	{
		ii = i-1;
		for (int j=1; j<= len;j++)
		{
			jj = j-1;
	 		gapa[i][j] = maxM( gapa[ii][j] + GAP_EXTENSION_SCORE, score[ii][j]+GAP_OPEN_AND_EXT_SCORE);
	 		gapb[i][j] = maxM( gapb[i][jj] + GAP_EXTENSION_SCORE, score[i][jj]+GAP_OPEN_AND_EXT_SCORE);
			score[i][j] = maxM(0,(maxM(score[ii][jj], maxM(gapa[ii][jj], gapb[ii][jj])) + (cost(ref[ii],ass[jj],MATCH_SCORE,MISMATCH_SCORE))));
		}
	}

	//print_matrix("score", ref, ass, score);
	//print_matrix("gapa", ref, ass, gapa);
	//print_matrix("gapb", ref, ass, gapb);
	
	//backtracking
	int max = -1;
	int pi, pj=len;
	for (int i=1; i<=ref.length(); i++)
	{
		if (score[i][len] > max)
		{
			max = score[i][len];
			pi = i;
		}
	}

	p_end = pi-1;
	p_end_full = p_end;
	a_end = pj-1;

	a = b = c  = "";

	int cur = 0;

	int match=0, mismatch=0, indel=0, anchor=0, anchor_len=0;
	int mismatch_full=0, indel_full=0;

	int s_match=0, s_mismatch=0, s_indel=0;
 
    bool end_set = false;

	while (score[pi][pj] > 0 || cur != 0)
	{
		if ( cur == 0 )
		{
			tmp = ((ref[pi-1] == ass[pj-1])?MATCH_SCORE:MISMATCH_SCORE);
			a = ref[pi-1] + a;
			b = ass[pj-1] + b;
			c = ((tmp>0)?'|':' ') + c;
			
			if(score[pi-1][pj-1] + tmp != score[pi][pj]){
				if (gapa[pi-1][pj-1] + tmp == score[pi][pj]) 
				{
					indel++;
					indel_full++;
					cur = 1;
				}
				else if (gapb[pi-1][pj-1] + tmp == score[pi][pj]) 
				{
					indel++;
					indel_full++;
					cur = 2;
				}
			}
			pi--;
			pj--;

			if (tmp > 0){
                anchor++;
                match++;
            }
            else{
                anchor = 0;
                mismatch++;
                mismatch_full++;
            }

            if(anchor >= anchor_len_min){
                if(!end_set){
                    p_end = pi + anchor_len_min - 1;
                    a_end = pj + anchor_len_min - 1;

                    end_set = true;
                    match = anchor_len_min;
                    mismatch = 0;
                    indel = 0;
                }
                if(anchor == anchor_len_min){
                	anchor_len += anchor;
                }
                else{
                	anchor_len++;
                }
                p_start = pi;
                a_start = pj;
                s_match = match;
                s_mismatch = mismatch;
                s_indel = indel;
            }

		}
		else if (cur == 1)
		{
			anchor = 0;
			a = ref[pi-1] + a;
			b = '-' + b;
			c = ' ' + c;
			if (score[pi-1][pj]+GAP_EXTENSION_SCORE+GAP_OPENING_SCORE == gapa[pi][pj])
				cur = 0;
			pi--;
		}
		else 
		{
			anchor = 0;
			a = '-' + a;
			b = ass[pj-1] + b;
			c = ' '+c;
			if (score[pi][pj-1]+GAP_EXTENSION_SCORE+GAP_OPENING_SCORE == gapb[pi][pj])
				cur = 0;
			pj--;
		}
	}

	p_start_full = pi;

	if(!end_set){
        p_start = pi;
        a_start = pj;
        identity = -999999;
    }
    else{
        int l = abs(((p_end_full-p_start_full+1)-len_full));
		identity_full = 1 - (mismatch_full+indel_full+log(l))/len_full;
    	
        match = s_match;
        mismatch = s_mismatch;
        indel = s_indel;
        len = a_end - a_start;
        l = 1+abs((((p_end-p_start)+1)-len));
		identity = 1 - (mismatch+indel+log(l))/len;
		SV_predicted_len = (ass.length() - anchor_len) + 1;
    }
}

void aligner::extract_calls(const string& ref, int start, int clusterId, int contigId, int min_identity, int min_support, int *coverage, FILE *fo)
{
	int g_pos = start;
	int r_pos = 1;
	int ev_gen_start, ev_gen_end, ev_ass_start, ev_ass_end ;
	int i=1;//0;
	double final_identity, var_len;
	string x, y, type;

	while (i<c.length())
	{
		if (c[i]=='|')
		{
			r_pos++;
			g_pos++;
			i++;
		}
		else 
		{
			if (a[i] != '-' && b[i] != '-')
			{
				ev_gen_start = ev_gen_end = g_pos;
				ev_ass_start = ev_ass_end = r_pos;
				r_pos++;
				g_pos++;
				x=a[i];
				y=b[i];
				i++;
			}
			else if (a[i] == '-')
			{
				
				ev_gen_start = g_pos-1;
				ev_ass_start = r_pos-1;
				x=a[i-1];
				y=b[i-1];
				
				while (a[i]=='-')
				{
					y+=b[i];
					r_pos++;
					i++;
				}

				ev_gen_end = ev_gen_start;
				ev_ass_end=r_pos-1;
			}
			else if (b[i]=='-')
			{
				ev_gen_start = g_pos-1;
				ev_ass_start = r_pos-1;
				x = a[i-1];
				y = b[i-1];

				while (b[i]=='-')
				{
					x+=a[i];
					g_pos++;
					i++;
				}
				ev_gen_end = g_pos-1;
				ev_ass_end = ev_ass_start;
			}
			int min = 10000, max = -1, sum = 0, avg = 0;
			for (int kk=ev_ass_start-1; kk<ev_ass_end; kk++)
			{
				if (coverage[kk]>max)
					max = coverage[kk];
				if (coverage[kk]<min)
					min = coverage[kk];

				sum += coverage[kk];
			}

			avg = sum/(ev_ass_end-ev_ass_start + 1);

			if(x.length() > y.length()){
				type = "DEL";
				var_len = x.length() - y.length();
				final_identity = identity_full + (1+log(var_len))/len_full; //one indel + gap size
			}
			else if(x.length() < y.length()){
				type = "INS";
				var_len = y.length() - x.length();
				final_identity = identity_full + (1+log(var_len))/len_full;  //one indel + gap size
				ev_gen_end = ev_gen_start;
			}
			else{
				type = "SNV";
				var_len = 1;
				final_identity = identity_full + var_len/len_full;
			}
	
			if(avg >= min_support && final_identity*100 >= min_identity && var_len > 0){ //var_len check is just a temp fix!!!
				fprintf(fo, "%s\t%d\t.\t%s\t%s\t%f\tPASS\tSVTYPE=%s;END=%d;Cluster=%d;Contig=%d;MinSupport=%d;AvgSupport=%d;MaxSupport=%d;Identity=%f\n", 
					ref.c_str(), ev_gen_start+1, x.c_str(), y.c_str(), (-10*log(1-final_identity)), type.c_str(), ev_gen_end+1, clusterId, contigId, min, avg, max, final_identity*100); 
			}
		}
	}
}

void aligner::dump(FILE *fo)
{
	string cnt = "";
	for (int i=1; i<=a.length(); i++)
	{
		if (i%10==0)
			cnt+='0';
		else if(i%5==0)
			cnt+='5';
		else
			cnt+=' ';
	}
	fprintf(fo, "   %s\nG: %s\n   %s\nA: %s\n",cnt.c_str(), a.c_str(), c.c_str(), b.c_str()); 
}

int aligner::get_start()
{
	return p_start;
}

int aligner::get_end()
{
	return p_end;
}

int aligner::get_anchor_start()
{
	return a_start;
}

int aligner::get_anchor_end()
{
	return a_end;
}

float aligner::get_identity()
{
	return 100*identity;
}

int aligner::get_SV_predicted_len()
{
	return SV_predicted_len;
}

void aligner::setAnchorLen(int len){
	anchor_len_min = len;
}

