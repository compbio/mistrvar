#include <algorithm>
#include <iostream>
#include <fstream>
#include <unordered_map>
#include <cstdio>
#include <string>
#include <map>
#include <zlib.h>
#include "partition.h"
#include "common.h"
#include "assembler.h"
#include "assembler_old.h"
#include "assembler_ext.h"
#include "mistrvar.h"
#include "genome.h"
#include "simulator.h"
#include "annotation.h"
#include "common.h"
#include "record.h"
#include "sam_parser.h"
#include "bam_parser.h"
#include "sort.h"

using namespace std;


// 10.46
// S:[step]:END

/********************************************************************/
inline string space (int i) 
{
	return string(i, ' ');
}

/********************************************************************/
inline string itoa (int i)
{
	char c[50];
	sprintf(c, "%d", i);
	return string(c);
}

/********************************************************************/
void partify (const string &read_file, const string &mate_file, const string &out, int threshold) 
{
	gzFile gzf = gzopen(mate_file.c_str(), "rb");
	unordered_map<string, string> mymap;

	const int MAXB = 8096;
	char buffer[MAXB];
	char name[MAXB], read[MAXB];
	while (gzgets(gzf, buffer, MAXB)) {
		sscanf(buffer, "%s %s", name, read);
		string read_name = string(name+1); 
		if (read_name.size() > 2 && read_name[read_name.size() - 2] == '/')
			read_name = read_name.substr(0, read_name.size() - 2);
		mymap[read_name] = string(read);
	}

	genome_partition pt(read_file, threshold, mymap); 

	FILE *fo = fopen(out.c_str(), "wb");
	FILE *fidx = fopen((out + ".idx").c_str(), "wb");
	while (pt.has_next()) {
		auto p = pt.get_next();
		size_t i = pt.dump(p, fo);
		fwrite(&i, 1, sizeof(size_t), fidx);
	}
	fclose(fo);
	fclose(fidx);
	printf("%d\n", pt.get_cluster_id());
}

/********************************************************************/
void assemble (const string &partition_file, const string &reference, const string &range, const string &out_vcf, const string &out_full,
					int global_mode, int max_len, int max_error, int min_support, int combined_identity, int single_identity, double final_identity, int ref_flank, const int LEGACY_ASSEMBLER)
{
	mistrvar predictor(partition_file, reference);
	predictor.run_mistrvar(range, out_vcf, out_full, global_mode, max_len, max_error, min_support, combined_identity, single_identity, final_identity, ref_flank, LEGACY_ASSEMBLER);
}

/********************************************************************/
bool isOEA (uint32_t flag)
{
	return (/*!flag ||*/ ((flag & 0x8) && (flag & 0x4)) 
				      || ((flag & 0x8) && !(flag & 0x4)) 
				      || (!(flag & 0x8) && (flag & 0x4)));
}

/********************************************************************/
bool isOEA_mrsFAST (uint32_t flag)
{
	return (/*!flag ||*/ ((flag & 0x8) && (flag & 0x4)) 
				      || ((flag & 0x8) && !(flag & 0x4)) 
				      || (!(flag & 0x8) && (flag & 0x4)));
}

/********************************************************************/
bool isClipped (const char *cigar, int error)
{

	int tmp = 0;

	while( *cigar )
	{
		if (isdigit(*cigar))
		{ tmp = 10 * tmp + (*cigar - '0');}
		else
		{
			if((*cigar == 'S' || *cigar == 'I' || *cigar == 'D') && tmp > error)return true;
			tmp = 0;
		}
		cigar++;
	}

	return false;
}

/********************************************************************/
bool isHighError (const char *attr, int error)
{

	string tags = string(attr);

	size_t prev = 0, pos;

	while ((pos = tags.find_first_of(" \t:", prev)) != string::npos){

		if (pos > prev){
			if(tags.substr(prev, pos-prev) == "NM"){
				prev = pos+1;
				pos = tags.find_first_of(":", prev);
				prev = pos+1;
				pos = tags.find_first_of(" \t:", prev);
				if(stoi(tags.substr(prev, pos-prev)) > error)return true;
			}
		}
		prev = pos+1;
	}
	
	return false;
}

/********************************************************************/
void extractOEA (const string &path, const string &result, int errors, bool fasta = false) 
{
	int mate_length = 500, tl = 0;
	int reversed;
	string seq1, q1, seq2, q2;

	FILE *fi = fopen(path.c_str(), "rb");
	unordered_map<string, Record> reads;
	char mc[2];
	fread(mc, 1, 2, fi);
	fclose(fi);

	Parser *parser;
	if (mc[0] == char(0x1f) && mc[1] == char(0x8b)) 
		parser = new BAMParser(path);
	else
		parser = new SAMParser(path);

	string comment = parser->readComment();
	string record1, record2, lastname;

	gzFile output = gzopen(result.c_str(), "wb6");
	//if (!fasta)
	//	gzwrite(output, comment.c_str(), comment.size());
	while (parser->hasNext()) {

		Record rc1 = parser->next();
		uint32_t flag1 = rc1.getMappingFlag();
		string readName1 = string(rc1.getReadName());
		
		unordered_map<string, Record >::iterator it = reads.find( readName1 );

//cerr << flag1 << ": " << isOEA(flag1) << "\t" << string(rc1.getCigar()) << ": " << isClipped(rc1.getCigar(), errors) << "\t" << string(rc1.getOptional()) << ": "  << isHighError(rc1.getOptional(), errors) << endl;
		if( flag1 < 256 && ( flag1 & 0x1 ) ){
			if(it == reads.end()){
					reads[readName1] = rc1;
			}
			else{
				Record rc2 = it->second; 
				uint32_t flag2 = rc2.getMappingFlag();

				if ( ( flag1 < 256 && ( flag1 & 0x1 ) && ( isOEA(flag1) || isClipped(rc1.getCigar(), errors) || isHighError(rc1.getOptional(), errors) ) ) ||
					 ( flag2 < 256 && ( flag2 & 0x1 ) && ( isOEA(flag2) || isClipped(rc2.getCigar(), errors) || isHighError(rc2.getOptional(), errors) ) ) ) { 
						
						reversed = ((flag1 & 0x10) == 0x10);
						seq1 = (reversed)? reverse_complement( rc1.getSequence() ): rc1.getSequence();
						q1   = (reversed)? reverse( rc1.getQuality() ) : rc1.getQuality();
						reversed = ((flag2 & 0x10) == 0x10);
						seq2 = (reversed)? reverse_complement( rc2.getSequence() ): rc2.getSequence();
						q2   = (reversed)? reverse( rc2.getQuality() ) : rc2.getQuality();

					if (fasta) {
						record1 = S("@%s\n%s\n+\n%s\n", rc1.getReadName(), seq1.c_str(), q1.c_str() );
						record2 = S("@%s\n%s\n+\n%s\n", rc2.getReadName(), seq2.c_str(), q2.c_str() ); 
						//record2 = S("@%s\n%s\n+\n%s\n", rc2.getReadName(), rc2.getSequence(), rc2.getQuality());
					}
					else {
						char *readName = (char*)rc1.getReadName();
						int l = strlen(readName);
						if (l > 1 && readName[l - 2] == '/')
							readName[l - 2] = '\0';
						record1 = S("%s %d %s %d %d %s %s %d %d %s %s %s\n",
							readName,
							rc1.getMappingFlag(),
							rc1.getChromosome(),
							rc1.getLocation(),
							rc1.getMappingQuality(),
							rc1.getCigar(),
							rc1.getPairChromosome(),
							rc1.getPairLocation(),
							rc1.getTemplateLength(),
							seq1.c_str(),
							q1.c_str(),
							rc1.getOptional()
			        		);

						readName = (char*)rc2.getReadName();
						l = strlen(readName);
						if (l > 1 && readName[l - 2] == '/')
							readName[l - 2] = '\0';
						reversed = ((flag2 & 0x10) == 0x10);
						record2 = S("%s %d %s %d %d %s %s %d %d %s %s %s\n",
							readName,
							rc2.getMappingFlag(),
							rc2.getChromosome(),
							rc2.getLocation(),
							rc2.getMappingQuality(),
							rc2.getCigar(),
							rc2.getPairChromosome(),
							rc2.getPairLocation(),
							rc2.getTemplateLength(),
							seq2.c_str(),
							q2.c_str(),
							rc2.getOptional()
			        		);
					}
					gzwrite(output, record1.c_str(), record1.size());
					gzwrite(output, record2.c_str(), record2.size());
					tl = min(strlen( rc1.getSequence() ), strlen( rc2.getSequence() ));
					if ( tl < mate_length) { mate_length = tl;}
				}
				reads.erase( it );
			}
		}
		parser->readNext();
	}

	gzclose(output);
	delete parser;

	FILE *fo = fopen( (result + ".length" ).c_str(), "w");
	fprintf(fo, "%d\n", mate_length);
	fclose(fo);

}

/********************************************************************/
void mask (const string &repeats, const string &path, const string &result, int pad = 0, bool invert = false)
{
	const int MAX_BUF_SIZE = 2000;
	size_t b, e, m = 0;
	char ref[MAX_BUF_SIZE], name[MAX_BUF_SIZE], l[MAX_BUF_SIZE];
	
	map<string, vector<pair<size_t, size_t>>> masks;
	FILE *fi = fopen(repeats.c_str(), "r");
	int prev = 0; string prev_ref = "";
	while (fscanf(fi, "%s %lu %lu %s", ref, &b, &e, name) != EOF) {
		if (e - b < 2 * pad) continue;
		masks[ref].push_back({b + pad, e - pad});
	}
	for (auto &r: masks) 
		sort(r.second.begin(), r.second.end());
	if (invert) for (auto &r: masks) 
	{
		vector<pair<size_t, size_t>> v;
		size_t prev = 0;
		for (auto &p: r.second) {
			if (prev < p.first)
				v.push_back({prev, p.first});
			prev = p.second;
		}
		v.push_back({prev, 500 * 1024 * 1024});
		r.second = v;
	}
	fclose(fi);
	
	const int MAX_CHR_SIZE = 300000000;
	char *x = new char[MAX_CHR_SIZE];

	fi = fopen(path.c_str(), "r");
	FILE *fo = fopen(result.c_str(), "w");
	fgets(l, MAX_BUF_SIZE, fi);
	
	while (true) {
		if (feof(fi))
			break;
		
		fprintf(fo, "%s", l);
		char *p = l; while (*p && !isspace(*p)) p++; *p = 0;
		string n = string(l + 1);
		printf("Parsed %s\n", n.c_str());
		
		size_t xlen = 0;
		while (fgets(l, MAX_BUF_SIZE, fi)) {
			if (l[0] == '>') 
				break;
			memcpy(x + xlen, l, strlen(l) - 1);
			xlen += strlen(l) - 1;
		}
		x[xlen] = 0;

		for (auto &r: masks[n]) {
			if (r.first >= xlen) continue;
			if (r.second >= xlen) r.second = xlen;
			memset(x + r.first, 'N', r.second - r.first);
		}

		for (size_t i = 0; i < xlen; i += 120) 
			fprintf(fo, "%.120s\n", x + i);
	}

	fclose(fi);
	fclose(fo);
	delete[] x;
}

/********************************************************************/
void removeUnmapped (const string &path, const string &result)
{
	ifstream fin(path.c_str());
	FILE *fo = fopen(result.c_str(), "w");
	
	string l, a, b, aa, bb; 
	while (getline(fin, a)) {
		getline(fin, b);
		getline(fin, l);
		getline(fin, l);

		int ll = a.size();
		if (ll > 1 && a[ll - 2] == '/')
			a = a.substr(0, ll - 2);
		if (a == aa)
			continue;
		else {
			fprintf(fo, "%s %s\n", aa.c_str(), bb.c_str());
			aa = a, bb = b;
		}
	}
	fprintf(fo, "%s %s\n", aa.c_str(), bb.c_str());

	fin.close();
	fclose(fo);
}

/********************************************************************/
void sortSAM (const string &path, const string &result) 
{
	sortFile(path, result, 2 * GB);
}

/********************************************************************/
void copy_string_reverse( char *src, char *dest)
{
	int limit=strlen(src);
	for( int i = 0; i < limit; i ++)
	{
		dest[i]=src[limit-1-i];
	}
	dest[limit]='\0';
}

/**********************************************/
void copy_string_rc( char *src, char *dest)
{
	int limit=strlen(src);
	for( int i = 0; i < limit; i ++)
	{
		switch( src[limit-1-i])
		{
			case 'A':
				dest[i]='T';
				break;
			case 'C':
				dest[i]='G';
				break;
			case 'G':
				dest[i]='C';
				break;
			case 'T':
				dest[i]='A';
				break;
			default:
				dest[i]='N';
				break;
		}
	}
	dest[limit]='\0';
}

/********************************************************************/
void extractMrsFASTOEA (const string &sam, const string &out) {
	string s;
	ifstream fin(sam.c_str());
	char name1[300], seq1[300], qual1[300];
	char name2[300], seq2[300], qual2[300];
	char seq1_new[300], qual1_new[300];
	char seq2_new[300], qual2_new[300];
	int flag1, flag2;
	FILE *fm = fopen(string(out + ".mapped.fq").c_str(), "wb");
	FILE *fu = fopen(string(out + ".unmapd.fq").c_str(), "wb");

	while (getline(fin, s)) {
		if (s[0] == '@') continue;
		sscanf(s.c_str(), "%s %d %*s %*d %*s %*s %*s %*s %*s %s %s",
				name1, &flag1, seq1, qual1);

		getline(fin, s);
		sscanf(s.c_str(), "%s %d %*s %*d %*s %*s %*s %*s %*s %s %s",
				name2, &flag2, seq2, qual2);

		if (flag1 == 0 && flag2 == 4)  {
			if (checkNs(seq2)) wo(fm, name1, seq1, qual1), wo(fu, name2, seq2, qual2);
		}
		if (flag1 == 16 && flag2 == 4) {
			copy_string_reverse(qual1, qual1_new);
			copy_string_rc(seq1, seq1_new);
			if (checkNs(seq2)) wo(fm, name1, seq1_new, qual1_new), wo(fu, name2, seq2, qual2);
		}
		if (flag1 == 4 && flag2 == 0)  {
			if (checkNs(seq1)) wo(fu, name1, seq1, qual1), wo(fm, name2, seq2, qual2);
		}
		if (flag1 == 4 && flag2 == 16) {
			copy_string_reverse(qual2, qual2_new);
			copy_string_rc(seq2, seq2_new);
			if (checkNs(seq1)) wo(fu, name1, seq1, qual1), wo(fm, name2, seq2_new, qual2_new);
		}
	}
	fclose(fm), fclose(fu);
	fin.close();
}

/********************************************************************/
void simulate_SVs (string in_file, string ref_file, bool from_bed) {

	simulator sim(ref_file);

	if(from_bed){
		sim.simulate(in_file);
	}
	else{
		sim.simulate_from_file(in_file);
	}
	
}

/********************************************************************/
void annotate (string gtf, string in_file, string out_file, bool genomic) {

	string line, chr, best_gene_s, best_trans_s, best_gene_e, best_trans_e, context_s, context_e, key, gene_id, trans_id;
	int start, end, t_start, t_end;
	ifstream infile (in_file);
	ofstream outfile (out_file);
	vector<uint32_t> vec_best_s, vec_best_e;
	vector<string> tokens;
	map <string,vector<isoform>> iso_gene_map;
	map <string,vector<gene_data>> gene_sorted_map;
	map <string,vector<uint32_t>> vec_all_s, vec_all_e;
	vector<string> contexts ={"intergenic", "intronic", "non-coding", "UTR", "exonic-CDS"};
	ensembl_Reader(gtf.c_str(), iso_gene_map, gene_sorted_map );

	if (infile.is_open()){
		while ( getline (infile,line)){

			tokens = split(line, '\t'); 
			chr = tokens[0];
			start = atoi(tokens[1].c_str());
			end = atoi(tokens[2].c_str());

			if(genomic){
				locate_interval(chr, start, start, gene_sorted_map[chr], 0, iso_gene_map, best_gene_s, best_trans_s, vec_best_s, vec_all_s);
				locate_interval(chr, end, end, gene_sorted_map[chr], 0, iso_gene_map, best_gene_e, best_trans_e, vec_best_e, vec_all_e);
			}
			else{
				gene_id = tokens[3];
				trans_id = tokens[4];

				locate_interval(chr, start, start, gene_id, trans_id, gene_sorted_map[chr], 0, iso_gene_map, best_gene_s, best_trans_s, vec_best_s, vec_all_s);
				locate_interval(chr, end, end, gene_id, trans_id, gene_sorted_map[chr], 0, iso_gene_map, best_gene_e, best_trans_e, vec_best_e, vec_all_e);
			}
			
			for (auto & match : vec_all_s){
				key = match.first;
				if(!vec_all_e[key].empty()){

					t_start = match.second[1];
					t_end = vec_all_e[key][1];

					context_s = contexts[vec_best_s[0]];
					context_e = contexts[vec_best_e[0]];

					if(t_start < t_end){
						outfile << chr << "\t" << start << "\t" << end << "\t" << key << "\t" << t_start << "\t" << t_end << "\t" << context_s << "\t" << context_e << endl;
					}
					else if(t_start > t_end){
						outfile << chr << "\t" << start << "\t" << end << "\t" << key << "\t" << t_end << "\t" << t_start << "\t" << context_e << "\t" << context_s << endl;
					}
					else{
						outfile << chr << "\t" << start << "\t" << end << "\t" << key << "\tmissing\tmissing\tNA\tNA" << endl;
					}
				}
				else{
					outfile << chr << "\t" << start << "\t" << end << "\t" << key << "\tmissing\tmissing\tNA\tNA" << endl;
				}
			}

			vec_best_s.clear();
			vec_best_e.clear();
			vec_all_s.clear();
			vec_all_e.clear();
		}
		infile.close();
		outfile.close();
	}

	
}

/********************************************************************/
int main(int argc, char **argv)
{
	try {
		if (argc < 2) throw "Usage:\tsniper [mode=(?)]";

		string mode = argv[1];
		if (mode == "fastq") {
			if (argc < 4) throw "Usage:\tsniper fastq [sam-file] [output] [errors]";
			extractOEA(argv[2], argv[3], atoi(argv[4]), argc == 5 ? true : false);
		}
		else if (mode == "oea") {
			if (argc != 4) throw "Usage:\tsniper oea [sam-file] [output]";
			extractMrsFASTOEA(argv[2], argv[3]);
		}
		else if (mode == "mask" || mode == "maski") {
			if (argc != 6) throw "Usage:\tsniper mask/maski [repeat-file] [reference] [output] [padding]";
			mask(argv[2], argv[3], argv[4], atoi(argv[5]), mode == "maski");
		}
		else if (mode == "sort") {
			if (argc != 4) throw "Usage:\tsniper sort [sam-file] [output]";
			sortSAM(argv[2], argv[3]);
		}
		else if (mode == "rm_unmap") {
			if (argc != 4) throw "Usage:\tsniper rm_unmap [fq-file] [output]";
			removeUnmapped(argv[2], argv[3]);
		}
		else if (mode == "partition") {
			if (argc != 6) throw "Usage:\tsniper partition [read-file] [mate-file] [output-file] [threshold]";
			partify(argv[2], argv[3], argv[4], atoi(argv[5]));
		}
		else if (mode == "assemble") {
			if (argc != 16) throw "Usage:\tsniper assemble [partition-file] [reference] [range] [output-file-vcf] [output-file-full] [mode] [max-len] [max-error] [min-support] [combined-identity] [single-identity] [final-identity] [reference-flank] [local-assembly]"; 
			assemble(argv[2], argv[3], argv[4], argv[5], argv[6], atoi(argv[7]), atoi(argv[8]), atoi(argv[9]), atoi(argv[10]), atoi(argv[11]), atoi(argv[12]), atof(argv[13]), atoi(argv[14]), atoi(argv[15]));
		}
		else if (mode == "get_cluster") {
			if (argc != 4) throw "Usage:\tsniper get_cluster [partition-file] [range]";
			genome_partition pt;
			pt.output_partition( argv[2], argv[3]);
		}
		else if (mode == "simulate_SVs") {
			if (argc != 5) throw "Usage:\tsniper simulate_SVs [bed-or-SV-file] [ref-file] [is-bed]";
			simulate_SVs(argv[2], argv[3], atoi(argv[4]));
		}
		else if (mode == "annotate") {
			if (argc != 6) throw "Usage:\tsniper annotate [gtf-file] [bed-file] [out-file] [is-genomic]";
			annotate(argv[2], argv[3], argv[4], atoi(argv[5]));
		}
		else {
			throw "Invalid mode selected";
		}
	}
	catch (const char *e) {
		ERROR("Error: %s\n", e);
		exit(1);
	}
		
	return 0;
}
